import { FunctionContext } from '@themost/data/functions';
import _ from 'lodash';
/**
 * Returns the current academic year based based on the department associated with the current context (e.g. the department of a student etc).
 */
_.assign(FunctionContext.prototype, {
    /**
     * @this FunctionContext
     * @returns {Promise<any>}
     */
    student() {
        const self = this;
        return new Promise((resolve, reject) => {
            self.model.resolveMethod('student', [], function (err, result) {
                if (err) {
                    return reject(err);
                }
                return resolve(result);
            });
        });
    },
    /**
     * Returns the current academic year based on the department associated with the current context (e.g. the department of a student etc).
     * @returns {Promise<any>}
     */
    currentYear() {
        let self = this;
        return new Promise((resolve, reject) => {
            self.model.resolveMethod('currentYear', [], function(err, result) {
                if (err) {
                    return reject(err);
                }
                resolve(result);
            });
        });
    },
    /**
     * Returns the current academic period based on the department associated with the current context (e.g. the department of a student etc).
     * @returns {Promise}
     */
    currentPeriod() {
        let self = this;
        return new Promise((resolve, reject) => {
            self.model.resolveMethod('currentPeriod', [], function(err, result) {
                if (err) {
                    return reject(err);
                }
                resolve(result);
            });
        });
    },
    /**
     * Returns an instructor identifier if the current context interactive user is an instructor
     * @returns {Promise<any>}
     */
    instructor() {
        let self = this;
        return new Promise((resolve, reject) => {
            self.model.resolveMethod('instructor', [], function(err, result) {
                if (err) {
                    return reject(err);
                }
                resolve(result);
            });
        });
    },
    /**
     * Returns the department identifier associated with the current context (e.g. the department of a student etc).
     * @returns {Promise}
     */
    department() {
        let self = this;
        return new Promise((resolve, reject) => {
            self.model.resolveMethod('department', [], function(err, result) {
                if (err) {
                    return reject(err);
                }
                resolve(result);
            });
        });
    }
});

