import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';
/**
 * @class
 */
declare class CourseStructureType extends DataObject {

     
     /**
      * @description Id
      */
     public id: number; 
     
     /**
      * @description The name of the item.
      */
     public name: string; 
     
     /**
      * @description Course structure type complete
      */
     public isComplete: boolean; 

}

export = CourseStructureType;