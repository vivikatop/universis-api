import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from "@themost/data/data-object";
@EdmMapping.entityType('Instructor')
/**
 * @class
 * @augments DataObject
 */
class Instructor extends DataObject {

    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * @swagger
     * /api/Instructors/Me:
     *  get:
     *    tags:
     *      - Instructor
     *    description: Returns an instructor which is associated with the current logged in user
     *    security:
     *      - OAuth2:
     *          - teachers
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/Instructor'
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    @EdmMapping.func("Me","Instructor")
    static getMe(context) {
        const model = context.model('Instructor');
        return new Promise(((resolve, reject) => {
            model.filter("id eq instructor()", (err, q) => {
                if (err) {
                    return reject(err);
                }
                return resolve(q);
            });
        }));
    }

    @EdmMapping.func("classes",EdmType.CollectionOf("CourseClass"))
    getInstructorClasses() {
        return this.context.model('CourseClass').where('instructors/instructor').equal(this.getId()).prepare();
    }

    @EdmMapping.func("currentClasses",EdmType.CollectionOf("CourseClass"))
    getCurrentClasses() {
        return this.context.model('CourseClass').where('instructors/instructor').equal(this.getId())
            .and('year').equal('$it/course/department/currentYear')
            .and('period').equal('$it/course/department/currentPeriod')
            .prepare();
    }

    @EdmMapping.func("exams",EdmType.CollectionOf("CourseExam"))
    getInstructorExams() {
        return this.context.model('CourseExam').where('instructors/instructor').equal(this.getId()).prepare();
    }

    @EdmMapping.func("currentExams",EdmType.CollectionOf("CourseExam"))
    getCurrentExams() {
        return this.context.model('CourseExam').where('instructors/instructor').equal(this.getId())
            .and('year').equal('$it/course/department/currentYear')
            .prepare();
    }

    @EdmMapping.func("theses",EdmType.CollectionOf("Thesis"))
    getInstructorTheses() {
        return this.context.model('Thesis').where('instructor').equal(this.getId()).prepare();
    }

    @EdmMapping.func("activeTheses",EdmType.CollectionOf("Thesis"))
    getInstructorActiveTheses() {
        return this.context.model('Thesis').where('instructor').equal(this.getId()).and('status/alternateName').equal('active').prepare();
    }

    @EdmMapping.func("classStudents",EdmType.CollectionOf("StudentCourseClass"))
    getInstructorClassStudents() {
        return this.context.model('StudentCourseClass').where('courseClass/instructors/instructor').equal(this.getId()).select('InstructorClassStudents').prepare();
    }

    @EdmMapping.func("thesisStudents",EdmType.CollectionOf("StudentThesis"))
    getInstructorThesisStudents() {
        return this.context.model('StudentThesis').where('thesis/instructor').equal(this.getId()).select('InstructorThesisStudents').prepare();
    }

}

module.exports = Instructor;