import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';
import GradeScaleValue = require('./grade-scale-value-model');

/**
 * @class
 */
declare class GradeScale extends DataObject {

     
     /**
      * @description Ο μοναδικός κωδικός της κλίμακας
      */
     public id: number; 
     
     /**
      * @description Η ονομασία του στοιχείου
      */
     public name: string; 
     
     /**
      * @description Ο τύπος της κλίμακας πχ. 0=Αριθμητική, 3=Κλίμακα τιμών κτλ
      */
     public scaleType: number; 
     
     /**
      * @description Ο παράγοντας της κλίμακας κατά την μετατροπή μίας τιμής
      */
     public scaleFactor: number; 
     
     /**
      * @description Η βάση της κλίμακας
      */
     public scaleBase: number; 
     
     /**
      * @description Ο αριθμός των δεκαδικών ψηφίων κατά την μορφοποίηση μίας τιμής
      */
     public formatPrecision?: number; 
     
     /**
      * @description Ο αριθμός των δεκαδικών ψηφίων κατά την μετατροπή μίας τιμής
      */
     public scalePrecision?: number; 
     
     /**
      * @description Μία συλλογή τιμών για την κλίμακα (αναφέρεται σε μη αριθμητικές κλίμακες)
      */
     public values?: Array<GradeScaleValue|any>; 

}

export = GradeScale;