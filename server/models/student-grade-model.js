import util from 'util';
import StudentMapper from './student-mapper';
import {ValidationResult} from "../errors";
import {DataObject} from "@themost/data/data-object";
import _ from 'lodash';
import {EdmMapping} from "@themost/data/odata";

@EdmMapping.entityType('StudentGrade')
/**
 * @class
 * @augments DataObject
 * @augments StudentMapper
 */
class StudentGrade extends DataObject {
    constructor() {
        super();
    }

    validate(callback) {
        const self = this, context = self.context;
        try {
            if (_.isNil(self.examGrade)) {
                return callback(null);
            }
            self.studentOf(function (err, result) {
                if (err) {
                    return callback(err);
                }
                if (_.isNil(result)) {
                    return callback(new Error('Student is null or undefined'));
                }
                /**
                 *
                 * @type {Student|DataObject|Array}
                 */
                const student = context.model('Student').convert(result);
                context.unattended(function(cb) {
                    student.is(':active').then(function (active) {
                        if (!active) {
                            return cb(new ValidationResult(false, 'ESTATUS', context.__('Student is not active.')));
                        }
                        else {
                            self.property('courseExam').select('course','year','examPeriod').silent().first(function(err, result) {
                                if (err) {
                                    return cb(err);
                                }
                                if (_.isNil(result)) {
                                    return cb(new Error('Course exam cannot be found.'));
                                }
                                const course=result.course, examYear=result.year, examPeriod=result.examPeriod;
                                const courses = context.model('Course');
                                courses.convert(course).replacedPassed(student.getId(), function (err, passed) {
                                    if (err) {
                                        return cb(err);
                                    }
                                    //get all passed grades for this student
                                    context.model('StudentGrade').where('student').equal(student.getId()).and('course').equal(course.id).and('isPassed').equal(1).expand('courseExam').silent().first(function(err,grade) {
                                        if (err) {
                                            return cb(err);
                                        }
                                        if(_.isNil(grade))
                                            return cb();
                                        else {
                                            // if exam grade passed is greater than grade scale base then error
                                            // if exam grade passed is less than grade scale base then Check year and courseExam period
                                            if ((!self.isPassed && examYear > grade.courseExam.year || (examYear === grade.courseExam.year && examPeriod > grade.courseExam.examPeriod))
                                                || (self.isPassed && examYear !== grade.courseExam.year && examPeriod !== grade.courseExam.examPeriod)) {
                                                return cb(new ValidationResult(false, 'EPASS', util.format(context.__("The specified course is already passed [%s-%s %s]"), grade.courseExam.year, grade.courseExam.year + 1, grade.courseExam.examPeriod)));
                                            }
                                            if (passed && self.isPassed) {
                                                // course
                                                return cb(new ValidationResult(false, 'EPASS', context.__("The specified course (or at least one of the courses replaced by this course) has been already passed.")));
                                            }
                                            return cb();
                                        }
                                    });
                                });
                            });

                        }
                    }).catch(function (err) {
                        cb(err);
                    });
                }, function(err) {
                    return callback(err);
                });

            });
        }
        catch (err) {
            callback(err);
        }
    }
}

StudentGrade.prototype.studentOf = StudentMapper.prototype.studentOf;

module.exports = StudentGrade;
