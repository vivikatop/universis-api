import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import User = require('./user-model');

/**
 * @class
 */
declare class CourseExamStatus extends DataObject {

     
     public id: number; 
     
     /**
      * @description Η ιδιότητα αναγνωριστικού που αντιπροσωπεύει οποιοδήποτε είδος αναγνωριστικού
      */
     public identifier?: number; 
     
     /**
      * @description An additional type for this item
      */
     public additionalType?: string; 
     
     /**
      * @description Ένα αναγνωριστικό όνομα για το στοιχείο.
      */
     public alternateName?: string; 
     
     /**
      * @description Μία σύντομη περιγραφή για το στοιχείο.
      */
     public description?: string; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση URL της εικόνας του στοιχείου.
      */
     public image?: string; 
     
     /**
      * @description Το όνομα του στοιχείου.
      */
     public name?: string; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση με περισσότερες πληροφορίες για το στοιχείο. Π.χ. τη διεύθυνση URL της σελίδας Wikipedia ή του επίσημου ιστότοπου.
      */
     public url?: string; 
     
     /**
      * @description Η ημερομηνία δημιουργίας του στοιχείου
      */
     public dateCreated?: Date; 
     
     /**
      * @description Η ημερομηνία τροποποίησης του στοιχείου
      */
     public dateModified?: Date; 
     
     /**
      * @description Ο χρήστης που δημιούργησε το στοιχείο.
      */
     public createdBy?: User|any; 
     
     /**
      * @description Ο χρήστης που τροποποίησε το στοιχείο.
      */
     public modifiedBy?: User|any; 

}

export = CourseExamStatus;