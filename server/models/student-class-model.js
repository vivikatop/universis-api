import _ from 'lodash';
import async from 'async';
import StudentMapper from './student-mapper';
import {DataObject} from "@themost/data/data-object";
import {ValidationResult} from "../errors";
import {LangUtils} from '@themost/common/utils';
import {EdmMapping} from "@themost/data/odata";
import {HttpNotFoundError} from "@themost/common";

@EdmMapping.entityType('StudentCourseClass')
/**
 * @class
 * @augments DataObject
 * @augments StudentMapper
 */
class StudentCourseClass extends DataObject {
    constructor() {
        super();
    }

    validateOnUpdate(callback) {
        return callback(null,[ new ValidationResult(true,'UPD',this.context.__('Student class registration does not have any rule due its state.'))]);
    }

    validateOnDelete(callback) {
        let self=this;
        // check institute configuration setting allowRemoveAutoRegisteredCourseClass
        return self.context.model('Student').select('id', 'department').expand({
            'name': 'department',
            'options':
                {
                    '$expand': 'organization($expand=instituteConfiguration)'

                }
        }).where('id').equal(self.student).getItem().then(function (result) {
            if (_.isNil(result)) {
                return new HttpNotFoundError();
            }
            // get allowRemoveAutoRegisteredCourseClass setting
            const allowRemoveAutoRegisteredCourseClass =
                result.department &&
                result.department.organization &&
                result.department.organization.instituteConfiguration &&
                result.department.organization.instituteConfiguration.allowRemoveAutoRegisteredCourseClass;

            if (allowRemoveAutoRegisteredCourseClass) {
                return callback(null, [new ValidationResult(true, 'DEL', self.context.__('Student class registration does not have any rule due its state.'))]);
            } else {
                //check if StudentCourseClass autoRegistered field
                if (self.autoRegistered) {
                    {
                        //check self.autoRegistered.alternateName !== 'student'
                        return self.context.model('AutoRegisterStatus').where('identifier').equal(self.autoRegistered).silent().getItem().then(function (status) {
                            if (_.isNil(status)) {
                                return callback(null, [new ValidationResult(true, 'DEL', self.context.__('Student class registration does not have any rule due its state.'))]);
                            }
                            if (status.alternateName !== 'student') {
                                return callback(null, [new ValidationResult(false, 'DEL', self.context.__('Removing student class registration is not allowed.'))]);
                            } else {
                                return callback(null, [new ValidationResult(true, 'DEL', self.context.__('Student class registration does not have any rule due its state.'))]);
                            }
                        });
                    }
                } else {
                    return callback(null, [new ValidationResult(true, 'DEL', self.context.__('Student class registration does not have any rule due its state.'))]);
                }
            }
        });
    }

    validate(callback) {
        const self = this, context = self.context;
        try {
            if (self.$state === 4) {
                return self.validateOnDelete(callback);
            }
            else if (self.$state === 2) {
                return self.validateOnUpdate(callback);
            }
            const rules = context.model('Rule');
            let validationResults;
            const additionalTypes = ['ClassRegistrationRule','ProgramCourseRegistrationRule','GroupRegistrationRule','SectionRegistrationRule'];
            const q=rules.where('target').equal(this.courseClass).and('additionalType').in(['ClassRegistrationRule']).prepare(true);

            q.or('target').equal(this.programCourse ? this.programCourse.toString() : null).and('additionalType').in(['ProgramCourseRegistrationRule']).prepare(true);
            //get program group rules
            if(LangUtils.parseInt(this.programGroup)!==0)
                q.or('target').equal(this.programGroup).and('additionalType').in(['GroupRegistrationRule']).prepare(true);
            //get class section rules if section selected
            if(LangUtils.parseInt(this.section)!==0 && LangUtils.parseInt(this.mustRegisterSection)!==0)
                q.or('target').equal(this.courseClass).and('additionalType').in(['SectionRegistrationRule']).prepare(true);

            q.silent().all(function(err, rulesData) {
                if (err) { return callback(err); }
                if (rulesData.length===0) { return callback(null, [new ValidationResult(true,'SUCC',context.__('Student class registration does not have any rule.'))]); }
                async.eachSeries(rulesData, function(item, cb) {
                    const ruleModel = context.model(item.refersTo + 'Rule');
                    if (_.isNil(ruleModel)) {
                        return callback(new Error(context.__('Student class registration rule type cannot be found.')));
                    }
                    const rule = ruleModel.convert(item);
                    rule.validate(self, function(err, result) {
                        if (err) { return callback(err); }
                        /**
                         * @type {ValidationResult[]}
                         */
                        validationResults = validationResults || [];
                        validationResults.push(result);
                        cb();
                    });
                }, function(err) {
                    if (err) { return callback(err); }

                    const fnValidateExpression = function(x) {
                        try {
                            let expr = x['ruleExpression'];
                            if (_.isEmpty(expr)) {
                                return false;
                            }
                            expr = expr.replace(/\[%(\d+)]/g, function () {
                                if (arguments.length === 0) return;
                                const id = parseInt(arguments[1]);
                                const v = validationResults.find(function (y) {
                                    return y.id === id;
                                });
                                if (v) {
                                    return v.success.toString();
                                }
                                return 'false';
                            });
                            expr = expr.replace(/\bAND\b/g, ' && ').replace(/\bOR\b/g, ' || ').replace(/\bNOT\b/g, ' !');
                            return eval(expr);
                        }
                        catch (e) {
                            return e;
                        }
                    };

                    const fnTitleExpression = function(x) {
                        try {
                            let expr = x['ruleExpression'];
                            if (_.isEmpty(expr)) {
                                return false;
                            }
                            expr = expr.replace(/\[%(\d+)]/g, function () {
                                if (arguments.length === 0) return;
                                const id = parseInt(arguments[1]);
                                const v = validationResults.find(function (y) {
                                    return y.id === id;
                                });
                                if (v) {
                                    return '(' + v.message.toString() + ')';
                                }
                                return 'Unknown Rule';
                            });
                            expr = expr.replace(/\bAND\b/g, context.__(' AND ')).replace(/\bOR\b/g, context.__(' OR ')).replace(/\bNOT\b/g, context.__(' NOT '));
                            return expr;
                        }
                        catch (e) {
                            return e;
                        }
                    };

                    const finalValidationResults = [];
                    additionalTypes.forEach(function(t) {
                        let res, title;
                        //filter by rule type
                       const filtered = rulesData.filter(function(x) {
                           return x['additionalType'] === t;
                       });
                       if (filtered.length>0) {
                           //apply default expression
                           const expr = filtered.find(function (x) {
                               return !_.isEmpty(x['ruleExpression']);
                           });
                           if (expr) {
                               res = fnValidateExpression(expr);
                               title = fnTitleExpression(expr);
                           }
                           else {
                               //get expression (for this rule type)
                               const ruleExp1 = filtered.map(function (x) {
                                   return '[%' + x.id + ']';
                               }).join(' AND ');
                               res = fnValidateExpression({ ruleExpression: ruleExp1 });
                               title = fnTitleExpression({ ruleExpression: ruleExp1 });
                           }
                           //build
                           let finalResult = new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
                           finalResult.type = t;
                           const childValidationResults = [];
                           filtered.forEach(function (x) {
                               childValidationResults.push.apply(childValidationResults, validationResults.filter(function (y) {
                                   return x.id === y.id;
                               }));
                           });
                           if (childValidationResults.length === 1) {
                               if (finalResult.success === childValidationResults[0].success) {
                                   finalResult = childValidationResults[0];
                               }
                               finalResult.type = t;
                           }
                           else {
                               finalResult.validationResults = childValidationResults;
                           }
                           finalValidationResults.push(finalResult);
                        }

                    });

                    callback(null, finalValidationResults)
                });
            });
        }
        catch (e) {
            callback(e);
        }
    }
}

StudentCourseClass.prototype.studentOf = StudentMapper.prototype.studentOf;

module.exports = StudentCourseClass;
