import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Student = require('./student-model');
import AcademicYear = require('./academic-year-model');
import AcademicPeriod = require('./academic-period-model');
import PeriodRegistrationType = require('./period-registration-type-model');
import PeriodRegistrationStatus = require('./period-registration-status-model');
import StudentCourseClass = require('./student-course-class-model');
import StudentRegistrationDocument = require('./student-registration-document-model');

/**
 * @class
 */
declare class StudentPeriodRegistration extends DataObject {

     
     /**
      * @description Μοναδικός κωδικός εγγραφής
      */
     public id: number; 
     
     /**
      * @description Φοιτητής
      */
     public student: Student|any; 
     
     /**
      * @description Ακαδημαϊκό έτος εγγραφής
      */
     public registrationYear: AcademicYear|any; 
     
     /**
      * @description Ακαδημαϊκή περίοδος εγγραφής
      */
     public registrationPeriod: AcademicPeriod|any; 
     
     /**
      * @description Ημερομηνία εγγραφής
      */
     public registrationDate?: Date; 
     
     /**
      * @description Ημερομηνία τροποποίησης
      */
     public registrationModifiedDate: Date; 
     
     /**
      * @description Τύπος εγγραφής περιόδου
      */
     public registrationType?: PeriodRegistrationType|any; 
     
     /**
      * @description Η κατάσταση της εγγραφής
      */
     public status: PeriodRegistrationStatus|any; 
     
     /**
      * @description Το εξάμηνο του φοιτητή
      */
     public semester: number; 
     
     /**
      * @description Αν έχει δίδακτρα (ΝΑΙ/ΟΧΙ)
      */
     public hasFees: number; 
     
     /**
      * @description Αν έχει δίδακτρα, έχει πληρωθεί (ΝΑΙ/ΟΧΙ)
      */
     public paid: number; 
     
     /**
      * @description Σημειώσεις
      */
     public notes?: string; 
     
     /**
      * @description Ημερομηνία τελευταίας τροποποίησης
      */
     public dateModified: Date; 
     
     /**
      * @description Το σύνολο των μαθημάτων που δηλώθηκαν
      */
     public classes?: Array<StudentCourseClass|any>; 
     
     /**
      * @description Το σύνολο των αποστολών μιας δήλωσης
      */
     public documents?: Array<StudentRegistrationDocument|any>; 

}

export = StudentPeriodRegistration;