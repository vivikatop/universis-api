import {EdmMapping,EdmType} from '@themost/data/odata';
import _ from 'lodash';
import {DataObject} from "@themost/data/data-object";
import {DataNotFoundError} from "@themost/common/errors";

@EdmMapping.entityType("Department")
/**
 * @class
 * @augments DataObject
 */
class Department extends DataObject {

    /**
     * @constructor
     */
    constructor() {
        super();
    }

    @EdmMapping.func("ActivePrograms",EdmType.CollectionOf("StudyProgram"))
    getActivePrograms() {
        return this.context.model('StudyProgram').where('department').equal(this.id).prepare();
    }

    /**
     * @param {number} year
     * @param {number} period
     * @returns {DataQueryable}
     */
    @EdmMapping.param('period', EdmType.EdmInt32, false)
    @EdmMapping.param('year', EdmType.EdmInt32, false)
    @EdmMapping.func("CourseClasses" ,EdmType.CollectionOf("CourseClass"))
    getCourseClasses(year, period) {
        return this.context.model('CourseClass')
            .where('course/department').equal(this.getId())
            .and('year').equal(year)
            .and('period').equal(period)
            .prepare();
    }

    @EdmMapping.func("CurrentClasses" ,EdmType.CollectionOf("CourseClass"))
    getCurrentClasses() {
        const self = this;
        return self.getModel().where('id').equal(self.id).select('currentYear','currentPeriod').getItem().then(function(result) {
            if (_.isNil(result)) {
                return Promise.reject(new DataNotFoundError("Department not found"));
            }
            return self.context.model('CourseClass')
                .where('course/department').equal(self.id)
                .and('year').equal(result.currentYear)
                .and('period').equal(result.currentPeriod)
                .prepare();
        });

    }

}

module.exports = Department;