import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Course = require('./course-model');
import Student = require('./student-model');
import AcademicYear = require('./academic-year-model');
import AcademicPeriod = require('./academic-period-model');
import Semester = require('./semester-model');
import CourseType = require('./course-type-model');
import ProgramGroup = require('./program-group-model');
import ExamPeriod = require('./exam-period-model');
import CourseStructureType = require('./course-structure-type-model');
import CourseExam = require('./course-exam-model');

/**
 * @class
 */
declare class StudentCourse extends DataObject {

     
     /**
      * @description Id
      */
     public id: string; 
     
     /**
      * @description Κωδικός μαθήματος
      */
     public course: Course|any; 
     
     /**
      * @description Κωδικός φοιτητή
      */
     public student: Student|any; 
     
     /**
      * @description Ακαδημαϊκό έτος βαθμολογίας
      */
     public gradeYear?: AcademicYear|any; 
     
     /**
      * @description Περίοδος βαθμολογίας
      */
     public gradePeriod?: AcademicPeriod|any; 
     
     /**
      * @description Τίτλος μαθήματος
      */
     public courseTitle?: string; 
     
     /**
      * @description Εξάμηνο μαθήματος
      */
     public semester: Semester|any; 
     
     /**
      * @description Κατεύθυνση μαθήματος
      */
     public specialty?: number; 
     
     /**
      * @description Διδακτικές μονάδες
      */
     public units?: number; 
     
     /**
      * @description Συντελεστής πτυχίου
      */
     public coefficient?: number; 
     
     /**
      * @description Βαθμός (0-1)
      */
     public grade?: number; 
     
     /**
      * @description Περιγραφή εξεταστικής περιόδου (μπορεί να περιέχει και ημερομηνία)
      */
     public gradePeriodDescription?: string; 
     
     /**
      * @description Τύπος μαθήματος
      */
     public courseType?: CourseType|any; 
     
     /**
      * @description Υπολογίζονται οι ΔΜ για τη λήψη πτυχίου
      */
     public calculateUnits: number; 
     
     /**
      * @description Υπολογίζεται στο βαθμό πτυχίου (ΝΑΙ/ΟΧΙ)
      */
     public calculateGrade: number; 
     
     /**
      * @description Τύπος: 0:Κανονική/-1:Απαλλαγή
      */
     public registrationType?: number; 
     
     /**
      * @description Σημειώσεις
      */
     public notes?: string; 
     
     /**
      * @description Κωδικός ομάδας προγράμματος σπουδών
      */
     public programGroup?: ProgramGroup|any; 
     
     /**
      * @description Ποσοστό συμμετοχής του βαθμού στην ομάδα
      */
     public groupPercent?: number; 
     
     /**
      * @description Κωδικός πατρικού μαθήματος
      */
     public parentCourse?: string; 
     
     /**
      * @description Ποσοστό συμμετοχής του βαθμού στο πατρικό μάθημα
      */
     public coursePercent?: number; 
     
     /**
      * @description Υπολογίζεται (ΝΑΙ/ΟΧΙ)
      */
     public calculated?: number; 
     
     /**
      * @description Κωδικός εξεταστικής περιόδου
      */
     public examPeriod?: ExamPeriod|any; 
     
     /**
      * @description Ώρες διδασκαλίας μαθήματος
      */
     public hours?: string; 
     
     /**
      * @description Μονάδες ECTS
      */
     public ects?: number; 
     
     /**
      * @description Ακαδημαϊκό έτος τελευταίας δήλωσης
      */
     public lastRegistrationYear?: AcademicYear|any; 
     
     /**
      * @description Ακαδημαϊκή περίοδος τελευταίας δήλωσης
      */
     public lastRegistrationPeriod?: AcademicPeriod|any; 
     
     /**
      * @description Ποσοστιαία βαθμολογία μαθήματος
      */
     public percentileRank?: number; 
     
     /**
      * @description Δομή μαθήματος
      */
     public courseStructureType?: CourseStructureType|any; 
     
     /**
      * @description Το μάθημα έχει περαστεί
      */
     public isPassed?: number; 
     
     /**
      * @description Ημερομηνία τελευταίας τροποποίησης
      */
     public dateModified: Date; 
     
     /**
      * @description Έχει ξαναδηλωθεί
      */
     public repeated: number; 
     
     /**
      * @description Βαθμός (ανηγμένος στην κλίμακα)
      */
     public formattedGrade?: string; 
     
     /**
      * @description Εξέταση βαθμολογίας
      */
     public gradeExam?: CourseExam|any; 

}

export = StudentCourse;