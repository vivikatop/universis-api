import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class

 * @property {number} id
 * @property {string} name
 * @property {string} alternateName
 * @property {string} identifier
 * @property {string} url
 * @property {boolean} local
 */
@EdmMapping.entityType('Institute')
class Institute extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * @swagger
     * /api/Institutes/Local:
     *  get:
     *    tags:
     *      - Instructor
     *    description: Returns local institute
     *    security:
     *      - OAuth2:
     *          - students
     *          - teachers
     *          - registrar
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/Institutes'
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    /**
     * @param {DefaultDataContext} context
     * @returns {Promise<DataQueryable>}
     */
    @EdmMapping.func('local', 'Institute')
    static async getLocal(context) {
        return await context.model('Institute')
            .where('local').equal(true);
    }

}
module.exports = Institute;
