import _ from 'lodash';
import {ValidationResult} from "../errors";
import {DataObject} from "@themost/data/data-object";
import {LangUtils} from '@themost/common/utils';
import {EdmMapping} from "@themost/data/odata";

@EdmMapping.entityType('Rule')
/**
 * @class
 * @param {*} obj
 * @constructor
 * @augments DataObject
 */
class Rule extends DataObject {

    constructor() {
        super();
        Object.defineProperty(this,'minExam',{
            get:function() {
                if (_.isEmpty(this.value5)) { return; }
                const arr = this.value5.split(';');
                if (_.isEmpty(arr[1])) { return; }
                return LangUtils.parseInt(arr[1]);
            }, configurable:false, enumerable:false
        });
        Object.defineProperty(this,'maxExam',{
            get:function() {
                if (_.isEmpty(this.value5)) { return; }
                const arr = this.value5.split(';');
                if (_.isEmpty(arr[0])) { return; }
                return LangUtils.parseInt(arr[0]);
            }, configurable:false, enumerable:false
        });
    }

    /**
     * @param {*} data
     * @param {Function} done
     */
    validate(data, done) {
        done = done || function() {};
        done(null, true);
    }

    /**
     * Returns the comparison operator associated with this rule
     * @returns {string}
     */
    operatorOf() {
        const op = LangUtils.parseInt(this.ruleOperator);
        switch (op) {
            case 0:return 'equal';
            case 1:return 'contains';
            case 2: return 'notEqual';
            case 3: return 'greaterThan';
            case 4: return 'lowerThan';
            case 5: return 'between';
            case 6: return 'startsWith';
            case 7: return 'notContains';
            case 8: return 'greaterOrEqual';
            case 9: return 'lowerOrEqual';
        }
    }

    /**
     *
     * @param {string} code
     * @param {string} message
     * @param {string=} innerMessage
     * @returns {ValidationResult}
     */
    success(code, message, innerMessage) {
        let sMessage = message;
        if (this.context) { sMessage = this.context.__(message); }
        const res = new ValidationResult(true, code, sMessage, innerMessage);
        res.id = this.id;
        return res;
    }

    /**
     *
     * @param {string} code
     * @param {string} message
     * @param {string=} innerMessage
     * @returns {ValidationResult}
     */
    failure(code, message, innerMessage) {
        let sMessage = message;
        if (this.context) { sMessage = this.context.__(message); }
        const res = new ValidationResult(false, code, sMessage, innerMessage);
        res.id = this.id;
        return res;
    }

    /**
     * @param {DataObject|Student} student
     * @param {Function} callback
     */
    excludeStudent(student, callback) {
        const self = this, context = self.context;
        //validate exceptions
        let q1 = context.model('Student').where('id').equal(student.getId()).prepare();
        if (self.minExam && self.maxExam) {
            q1.where('semester').lowerThan(self.minExam).or('semester').greaterThan(self.maxExam).prepare();
        }
        else if (self.minExam) {
            q1.where('semester').lowerThan(self.minExam).prepare();
        }
        else if (self.maxExam) {
            q1.where('semester').greaterThan(self.maxExam).prepare();
        }
        else {
            q1 = null;
        }
        if (_.isNil(q1)) {
            callback(null, false);
        }
        else {
            q1.silent().count(function(err, result) {
                if (err) { return callback(err); }
                callback(null, (result===1));
            });
        }
    }

    formatMessage(p) {
        //
    }

    formatDescription(callback) {
        return callback();
    }
}

module.exports = Rule;
