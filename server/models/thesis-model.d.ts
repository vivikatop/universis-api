import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Department = require('./department-model');
import AcademicYear = require('./academic-year-model');
import AcademicPeriod = require('./academic-period-model');
import Instructor = require('./instructor-model');
import ThesisType = require('./thesis-type-model');
import ThesisStatus = require('./thesis-status-model');
import GradeScale = require('./grade-scale-model');

/**
 * @class
 */
declare class Thesis extends DataObject {

     
     /**
      * @description Ο μοναδικός κωδικός της εργασίας
      */
     public id: number; 
     
     /**
      * @description Το τμήμα που προσφέρει την εργασία
      */
     public department: Department|any; 
     
     /**
      * @description Η ονομασία της εργασίας
      */
     public name?: string; 
     
     /**
      * @description Η ημερομηνία έναρξης της εργασίας
      */
     public startDate?: Date; 
     
     /**
      * @description Το ακαδημαϊκό έτος έναρξης της εργασίας
      */
     public startYear?: AcademicYear|any; 
     
     /**
      * @description Η ακαδημαϊκή περίοδος έναρξης της εργασίας
      */
     public startPeriod?: AcademicPeriod|any; 
     
     /**
      * @description Η ημερομηνία παράδοσης της εργασίας
      */
     public endDate?: Date; 
     
     /**
      * @description Ο επιστημονικός υπεύθυνος της εργασίας.
      */
     public instructor?: Instructor|any; 
     
     /**
      * @description Ο συντελεστής που συμμετέχει ο βαθμός της εργασίας στο βαθμό πτυχίου.
      */
     public coefficient?: number; 
     
     /**
      * @description Σημειώσεις
      */
     public notes?: string; 
     
     /**
      * @description Το θέμα της εργασίας
      */
     public subject?: number; 
     
     /**
      * @description Άλλοι εισηγητές
      */
     public otherInstructors?: string; 
     
     /**
      * @description Τύπος εργασίας
      */
     public type?: ThesisType|any; 
     
     /**
      * @description Κατάσταση εργασίας
      */
     public status: ThesisStatus|any; 
     
     /**
      * @description Βαθμός εργασίας
      */
     public grade?: number; 
     
     /**
      * @description Διδακτικές μονάδες
      */
     public units?: number; 
     
     /**
      * @description Η πιο διαδεδομένη βαθμολογική κλίμακα είναι η δεκαδική (από 0 έως 10 με βάση το 5), ωστόσο κάποια χρησιμοποιούν και άλλες βαθμολογικές κλίμακες (πχ. Λατινικά γράμματα, Επιτυχώς/Ανεπιτυχώς, δεκαδική από 0 έως 10 με βάση το 6 κ.α.) 
      */
     public gradeScale?: GradeScale|any; 
     
     /**
      * @description Μονάδες ECTS
      */
     public ects?: number; 
     
     /**
      * @description Η ημερομηνία διεξαγωγής της εξέτασης της εργασίας
      */
     public examDate?: Date; 
     
     /**
      * @description Ημερομηνία τροποποίησης
      */
     public dateModified: Date; 

}

export = Thesis;