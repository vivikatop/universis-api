import {DataObject} from '@themost/data/data-object';
import Institute = require('./institute-model');
import User = require('./user-model');

/**
 * @class
 */
declare class InstituteConfiguration extends DataObject {

     
     /**
      * @description An additional type for the item.
      */
     additionalType?: string;
     
     /**
      * @description An educational institute associated with this configuration
      */
     institute?: Institute|any;
     
     useStudentRegisterAction: boolean;
     
     /**
      * @description Μοναδικός κωδικός
      */
     id?: number;
     
     /**
      * @description Ένα αναγνωριστικό όνομα για το στοιχείο.
      */
     alternateName?: string; 
     
     /**
      * @description Μία σύντομη περιγραφή για το στοιχείο.
      */
     description?: string; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση URL της εικόνας του στοιχείου.
      */
     image?: string; 
     
     /**
      * @description Το όνομα του στοιχείου.
      */
     name?: string; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση με περισσότερες πληροφορίες για το στοιχείο. Π.χ. τη διεύθυνση URL της σελίδας Wikipedia ή του επίσημου ιστότοπου.
      */
     url?: string; 
     
     /**
      * @description Η ημερομηνία δημιουργίας του στοιχείου
      */
     dateCreated?: Date; 
     
     /**
      * @description Η ημερομηνία τροποποίησης του στοιχείου
      */
     dateModified?: Date; 
     
     /**
      * @description Ο χρήστης που δημιούργησε το στοιχείο.
      */
     createdBy?: User|any; 
     
     /**
      * @description Ο χρήστης που τροποποίησε το στοιχείο.
      */
     modifiedBy?: User|any;

    /**
     * @description show pending grades to students
     */
    showPendingGrades: boolean;

    /**
     * @description allow student to remove registrar course classes
     */
    allowRemoveAutoRegisteredCourseClass:boolean;

}

export = InstituteConfiguration;
