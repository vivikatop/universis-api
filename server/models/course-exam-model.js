import async from 'async';
import {ValidationResult} from "../errors";
import {DataObject} from "@themost/data/data-object";
import {TraceUtils } from '@themost/common/utils';
import {LangUtils} from '@themost/common/utils';
import _ from 'lodash';
import {EdmMapping, EdmType} from "@themost/data/odata";
import util from "util";
import {DataNotFoundError, HttpError, HttpNotFoundError, HttpServerError} from "@themost/common";
import {QueryEntity} from "@themost/query";

@EdmMapping.entityType('CourseExam')
/**
 * @class
 */
class CourseExam extends DataObject {

    constructor() {
        super();
    }

    save(context, callback) {
        try {
            const self = this;
            //ensure grades list
            self.grades = self.grades || [];

            //clear validation result
            delete self.validationResult;

            //get original classes
            self.validateState(function(err) {
                if (err) {
                    self.validationResult = new ValidationResult(false, err.code || 'EFAIL', self.context.__('Course exam status is not valid for editing.'), err.message);
                    return callback();
                }
                if (self.grades.length===0)
                {
                    self.validationResult = new ValidationResult(false, 'EFAIL', self.context.__('Course grades cannot be empty.'), err.message);
                    return callback();
                }
                else {

                    //get all course exam grades
                    const query = `SELECT * FROM [dbo].ufnCourseExamStudentGrades (${self.id})`;
                    context.db.execute(query, null, function (err, examGrades) {
                        if (err) {
                            return callback(err);
                        }
                        self.grades.forEach(function(x) {
                            try {
                                //clear validation result if any
                                delete x.validationResult;

                                const examGrade = examGrades.find(function (y) {
                                    return x.student === y.student;
                                });
                                //check if grade exists in exam grades
                                if (typeof examGrade === 'undefined') {
                                    x.validationResult = new ValidationResult(false, 'EAVAIL', 'The student specified is unavailable for the selected course exam.');
                                }
                                else {
                                    //check if grade is changed, remove if not changed
                                    if (examGrade.formattedGrade === x.formattedGrade) {
                                        //remove from grades
                                        x.status = 0;
                                    }
                                    else {
                                        //set values
                                        x.status = {
                                            "alternateName": "pending"
                                        }; //pending
                                        if (_.isNil(examGrade.status)) { //insert grade
                                            x.$state = 1;
                                            x.courseClass = examGrade.courseClass;
                                            if (_.isNil(x.formattedGrade))
                                                x.status = 0;
                                        }
                                        else { //update grade
                                            x.$state = 2;
                                            x.id = examGrade.id;
                                            if (_.isNil(x.formattedGrade) && (_.isNil(x.deleted) === true || x.deleted === false))
                                                x.status = 0;
                                        }
                                    }
                                }
                            }
                            catch (e) {
                                return callback(e);
                            }
                        });

                        context.db.executeInTransaction(function (cb) {
                            //get grade scale
                            context.model('CourseExam').select('id','gradeScale').expand('gradeScale').where('id').equal(self.id).first(function(err, courseExam) {
                                if (err) {
                                    return cb(err);
                                }
                                if (_.isNil(courseExam)){
                                    return callback(new Error(context.__('Course exam cannot be found.')));
                                }
                            async.eachSeries(self.grades, function (item, cb1) {
                                try {

                                    if (item.validationResult || item.status === 0) {
                                        if (item.status === 0) {
                                            item.validationResult = (new ValidationResult(true, 'NOACTION',  context.__('Student grade is not supplied or is the same.')));
                                            return cb1();
                                        }
                                        else
                                            return cb1();
                                    }
                                    context.db.execute('SELECT [dbo].[ufnConvertGrade] (?,?) as grade', [item.formattedGrade, courseExam.gradeScale.id], function (err, result) {
                                        if (err) {
                                            return cb1(err);
                                        }
                                        item.examGrade = result[0].grade;
                                        item.grade1 = item.examGrade;
                                        item.courseExam=self.id;

                                        if (_.isNil(item.examGrade) && !_.isNil(item.formattedGrade)) {
                                            item.validationResult = new ValidationResult(false, 'INVDATA', context.__("The specified grade is not valid."));
                                            return cb1();
                                        }
                                        item.isPassed = item.examGrade >= courseExam.gradeScale.scaleBase;

                                        //if validationResult is undefined and grade status=pending, continue with saving
                                        const studentGrade = context.model('StudentGrade').convert(item);
                                        studentGrade.validate(function (err) {
                                            if (err) {
                                                item.validationResult = new ValidationResult(false, 'INVDATA', err.message);
                                                return cb1();
                                            }
                                            studentGrade.save(context, function (err) {
                                                if (err) {
                                                    //if err is instance of ValidationResult
                                                    if (err instanceof ValidationResult) {
                                                        //push validation result to array
                                                        item.validationResult = err;
                                                    }
                                                    else {
                                                        //convert error to ValidationResult
                                                        item.validationResult = new ValidationResult(false, 'FAIL', 'An internal error occurred while saving grade.', err.message);
                                                    }
                                                }
                                                else {
                                                    item.validationResult = (new ValidationResult(true, 'SUCC', context.__('Student grade successfully saved.')));
                                                }
                                                //exit without error
                                                cb1();
                                            });
                                        });
                                    });
                                }
                                catch (er) {
                                    cb1(er);
                                }
                            }, function (err) {
                                cb(err);
                            });
                        });
                        }, function (err) {
                            if (err) {
                                if (err instanceof ValidationResult) {
                                    self.validationResult = err;
                                }
                                else {
                                    TraceUtils.error(err);
                                    self.validationResult = new ValidationResult(false, err.code || 'EFAIL', self.context.__('Course exam cannot be saved.'), err.message);
                                }
                                callback();
                            }
                            else {
                                const failed = self.grades.find(function (x) {
                                    if (typeof x.validationResult === 'undefined')
                                        return false;
                                    return !x.validationResult.success;
                                });
                                if (typeof self.validationResult === 'undefined') {
                                    if (typeof failed !== 'undefined') {
                                        self.validationResult = new ValidationResult(true, 'PSUCC', self.context.__('Course exam successfully saved but with errors.'));
                                    }
                                    else {
                                        self.validationResult = new ValidationResult(true, 'SUCC', self.context.__('Course exam successfully saved.'));
                                    }
                                    //add to event log
                                    context.model('CourseExam').where('id').equal(self.id).select( "id", "year","name", "examPeriod/name as examPeriod","course/displayCode as displayCode","course/name as courseName").flatten().silent().first(function (err, exam) {
                                        if (err) {
                                            return callback(err);
                                        }
                                        if (!_.isNil(exam)) {
                                            const eventTitle = `Ενημέρωση βαθμολογίου της εξεταστικής περιόδου ${exam.year}-${exam.year + 1} ${exam.examPeriod} του μαθήματος [${exam.displayCode}] ${exam.courseName}`;
                                            context.unattended(function (cb) {
                                                context.model('EventLog').save({title: eventTitle, eventType: 2, username: context.interactiveUser.name, eventSource: 'teachers', eventApplication: 'universis'}, function (err) {

                                                    if (err)
                                                    {
                                                        return cb(err);
                                                    }
                                                    const courseExam =  context.model('CourseExam').convert(exam);
                                                    return courseExam.createDocument(function (err, docId) {
                                                        self.docId = docId;
                                                        return context.model('CourseExam').save(self).then(() => {
                                                            return cb();
                                                        }).catch( err => {
                                                            return cb(err);
                                                        });
                                                    });
                                                });

                                            }, function (err) {
                                                if (err) {
                                                    TraceUtils.error(err);
                                                }
                                                callback();
                                            });
                                        }
                                        else {
                                            callback();
                                        }
                                    });
                                }
                            }

                        });
                    });


                }
            });
        }
        catch(er) {
            callback(er);
        }
    }

    validateState(callback) {
        try {
            const self = this, context = self.context;
            context.model('CourseExamInstructor').where('courseExam').equal(self.id).select( "courseExam/id as id", "courseExam/status/alternateName as status", "courseExam/completedByUser as completedByUser").flatten().silent().first(function (err, exam) {
                if (err) {
                    return callback(err);
                }
                if (!_.isNil(exam)) {
                    context.model('CourseExam').resolveMethod('me', [], function (err, user) {
                        if (err) {
                            return callback(err);
                        }
                        const previousExamStatus = exam["status"];
                        let status;
                        if (!self.status) {
                            if (previousExamStatus === "completed" || previousExamStatus==='open') {
                                //check if completedByUser is null or  same with logged in user
                                if (LangUtils.parseInt(exam["completedByUser"]) !== user)
                                    return callback(new Error(context.__('Course exam is in edit state by a different user.')));
                                else
                                    return callback(null);
                            }
                            else
                                return callback(new Error(context.__('Course exam cannot be saved due to its state.')));
                        }
                        else {
                            status = self.context.model('CourseExamStatus').convert(self.status).alternateName;
                            // allow only completed status to be set
                            if (status !== "completed") {
                                return callback(new Error(context.__('Course exam status is not valid.')));
                            }
                            if (previousExamStatus === "completed" || previousExamStatus === 'open') {
                                //set user
                                self.completedByUser = user;
                                self.dateCompleted = new Date();
                                return callback(null);
                            }
                            else {
                                // previous exam status does not allow status changing
                                return callback(new Error(context.__('Course exam status cannot be set due to its state.')));
                            }
                        }
                    });
                }
                else
                    return callback(new Error(context.__('Course exam cannot be found.')));
            });
        }
    catch(e) {
        callback(e);
    }
    }

    courseOf(callback) {
        this.attrOf('course', callback);
    }

    createDocument(callback) {
        try {
            const self = this, context = self.context;
            context.model('CourseExam').resolveMethod('me', [], function (err, user) {
                if (err) {
                    return callback(err);
                }
                const query = `sp_createCourseExamGradesDocument ${self.id},${user}`;
                context.db.execute(query, null, function (err, result) {
                    if (err) {
                        TraceUtils.error(err);
                        return callback(err);
                    }
                    else {
                        const docId = result[0].docId;
                        context.unattended(function (cb) {
                            context.model('CourseExamDocument').where('id').equal(docId).select( "id", "originalDocument").flatten().silent().first(function (err, exam) {
                                if (err) {
                                    TraceUtils.error(err);
                                    return cb(err);
                                }
                                if (_.isNil(exam)) {
                                    TraceUtils.error(`CourseExamDocument with id ${docId} cannot be found`);
                                    return cb(new Error(`CourseExamDocument with id ${docId} cannot be found`));
                                }
                                let crypto = require('crypto');
                                let shasum = crypto.createHash('sha1');
                                //add hash key
                                const doc = exam.originalDocument;
                                shasum.update(doc, 'utf8');
                                exam.checkHashKey = shasum.digest('base64');
                                context.model('CourseExamDocument').save(exam, function (err) {
                                    cb(err);
                                });
                            });
                        }, function (err) {
                            if (err) {
                                TraceUtils.error(err);
                            }
                            callback(err,docId);
                        });
                    }
                });
            });
        }
        catch (e) {
            callback(e);
        }
    }

    /**
     * @returns {Promise<*>}
     */
    async preCheck () {
        let self = this;
        try {
            /**
             * @type {DataContext}
             */
            let context = self.context;
            //ensure grades list
            self.grades = self.grades || [];
            if (self.grades.length === 0) {
                self.validationResult = new ValidationResult(false, 'EFAIL', self.context.__('Course grades cannot be empty.'));
                return;
            }
            // get all course exam grades
            let examGrades = await new Promise((resolve, reject) => {
                context.db.execute(`SELECT * FROM [dbo].ufnCourseExamStudentGrades (?)`, [self.id], (err, result) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(result);
                });
            });
            /**
             *
             * @type {GradeScale|any}
             */
            let gradeScale = await this.context.model('GradeScale').where('id').equal(this.gradeScale).silent().getTypedItem();
            if (typeof gradeScale === 'undefined') {
                throw new DataNotFoundError('Course grade scale cannot be found or is inaccessible.');
            }
            // enumerate grades

            let forEachGrade = async grade => {
                try {
                    let previousGrade = examGrades.find( examGrade => grade.student === examGrade.student );
                    if (typeof previousGrade === 'undefined') {
                        // student not found
                        grade.validationResult = new ValidationResult(true, 'EAVAIL', 'The student specified is unavailable for the selected course exam.');
                        return;
                    }
                    // set current grade
                    grade.grade1 = grade.examGrade = gradeScale.convertFrom(grade.formattedGrade);
                    if (grade.examGrade == previousGrade.examGrade) {

                        grade.validationResult = new ValidationResult(true, 'UNMOD', context.__('Grade has not been modified.'));
                    }
                    else {
                        if (typeof previousGrade.examGrade === 'undefined' || previousGrade.examGrade === null) {
                            // insert grade
                            grade.validationResult = new ValidationResult(true, 'INS', context.__('A new student grade will be inserted.'));
                        }
                        else {
                            // update grade
                            grade.validationResult = new ValidationResult(true, 'UPD', context.__('Student grade already exists and it will be replaced.'));
                        }
                    }
                    const studentGrade = context.model('StudentGrade').convert(grade);
                    return await new Promise((resolve) => {
                        studentGrade.validate(function (err) {
                            if (err) {
                                if (err instanceof ValidationResult) {
                                    grade.validationResult = err;
                                }
                                else {
                                    TraceUtils.error(err);
                                    grade.validationResult = new ValidationResult(false, 'EFAIL', context.__('An internal server error occurred.'), err.message);
                                }
                            }
                            return resolve();
                        });
                    });
                }
                catch (err) {
                    TraceUtils.error(err);
                    if (err instanceof RangeError) {
                        grade.validationResult = new ValidationResult(false, 'ERANGE', context.__(err.message));
                    }
                    else {
                        grade.validationResult = new ValidationResult(false, 'EFAIL', context.__('An internal server error occurred.'), err.message);
                    }
                }
            };

            await Promise.all(self.grades.map(grade => {
               return forEachGrade(grade);
            }));
            if (self.grades.find( x=> x.validationResult.success === false)) {
                self.validationResult = new ValidationResult(true, 'PSUCC', self.context.__('Course exam contains warnings-errors.'));
            }
            else {
                self.validationResult = new ValidationResult(true, 'SUCC', self.context.__('Course exam validation check was completed successfully.'));
            }
        }
        catch (err) {
            TraceUtils.error(err);
            self.validationResult = new ValidationResult(false, err.code || 'EFAIL', self.context.__('Course exam cannot be validated due to internal error.'), err.message);
        }
    }

    /**
     * @returns {Promise<DataQueryable>}
     */
    @EdmMapping.func("students",EdmType.CollectionOf("CourseExamStudentGrade"))
    getStudents() {
        const self = this;
        return self.context.model('CourseExamInstructor').where('courseExam').equal(self.id).expand('courseExam').first().then(function (courseExam) {
            if (_.isNil(courseExam)) {
                return Promise.reject(new HttpNotFoundError());
            }
            const params = Object.assign({}, self.context.params);
            delete params.id;
            const entityExpr = util.format('ufnCourseExamStudentGrades(%s)', self.id);
            const entity = new QueryEntity(entityExpr).as('CourseExamStudentGrades');
            const q = self.context.model('CourseExamStudentGrade').asQueryable();
            //replace entity reference
            q.query.from(entity);
            //return queryable
            return Promise.resolve(q.silent());
        }).catch(function (err) {
            TraceUtils.error(err);
            if (err instanceof HttpError) {
                return Promise.reject(err);
            }
            return Promise.reject(new HttpServerError());
        });
    }

    /**
     * @returns {DataQueryable}
     */
    @EdmMapping.func("actions",EdmType.CollectionOf("ExamDocumentUploadAction"))
    getDocumentActions() {
        return this.context.model('ExamDocumentUploadAction').where('object').equal(this.id).prepare();
    }

}

module.exports = CourseExam;
