import util from 'util';
import _ from 'lodash';
import { QueryEntity } from '@themost/query/query';
import {EdmMapping,EdmType} from '@themost/data/odata';
import {HttpServerError, HttpNotFoundError, HttpError, HttpForbiddenError} from '@themost/common/errors';
import {TraceUtils} from '@themost/common/utils';
import {DataObject} from "@themost/data/data-object";
import {LangUtils} from '@themost/common/utils';
import StudentRegisterActionModel from "./student-register-action-model";
import {ValidationResult} from "../errors";

@EdmMapping.entityType("Student")
/**
 * @class
 */
class Student extends DataObject {
    constructor() {
        super();
        const self = this;
        self.selector('active',function(callback) {
            try {
                self.attrOf('studentStatus', function(err, result) {
                    if (err) { return callback(err); }
                    const status = self.context.model('StudentStatus').convert(result).getId();
                    callback(null, (status===1));
                });

            }
            catch(er) {
                callback(er);
            }
        });
    }

    programOf(callback) {
        this.attrOf('studyProgram', callback);
    }

    departmentOf(callback) {
        this.attrOf('department', callback);
    }

    inferSemester(year, period, callback) {
        const self=this, context=self.context;
        const query = `select dbo.ufnCalculateStudentSemester (${year},${period},${self.getId()}) as semester`;
        context.db.execute(query, null, function (err, result) {
            if (err) {
                TraceUtils.error(err);
                callback(err);
            }
            callback(null,result[0]["semester"]);
        });
    }

    /**
     * Returns a data queryable for the current student
     * @param {ExpressDataContext} context
     * @returns {DataQueryable}
     */
    @EdmMapping.func("Me", "Student")
    static getMe(context) {
        return context.model('Student').where('user/name').notEqual(null)
            .and('user/name').equal(context.user.name)
            .prepare();
    }

    /**
     *
     * @param {ExpressDataContext} context
     * @return {DataQueryable}
     */
    @EdmMapping.func("Active",EdmType.CollectionOf("Student"))
    static getActiveStudents(context) {
        return context.model('Student').where('studentStatus').equal(1).prepare();
    }


    /**
     *
     */
    @EdmMapping.func("LastPeriodRegistration","StudentPeriodRegistration")
    getLastPeriodRegistration() {
        return this.context.model('StudentPeriodRegistration').where("student").equal(this.getId()).orderByDescending("registrationYear").thenByDescending("registrationPeriod").getItem();
    }

    /**
     *
     */
    @EdmMapping.func("Registrations",EdmType.CollectionOf("StudentPeriodRegistration"))
    getRegistrations() {
        return this.context.model('StudentPeriodRegistration').where("student").equal(this.getId()).prepare();
    }

    /**
     *
     */
    @EdmMapping.func("Courses",EdmType.CollectionOf("StudentCourse"))
    getCourses() {
        return this.context.model('StudentCourse').where("student").equal(this.getId()).prepare();
    }

    /**
     * @returns Promise<DataQueryable>
     */
    @EdmMapping.func("Requests",EdmType.CollectionOf("RequestAction"))
    getRequests() {
        return new Promise((resolve, reject) => {
            return this.context.model('RequestAction').filter({
                $filter: "owner eq me()"
            }, (err, query)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(query);
            });
        });
    }

    /**
     *
     */
    @EdmMapping.func("Grades",EdmType.CollectionOf("StudentGrade"))
    getGrades() {
        const self = this;
        const context=self.context;
        // get student department organization configuration
        return self.getModel().select('id', 'department').expand({
                'name':'department',
                'options':
                    {'$expand':'organization($expand=instituteConfiguration)'

                    }
            }).where('id').equal(self.id).getItem().then(function (result) {
            if (_.isNil(result)) {
                return new HttpNotFoundError();
            }
            // get showPendingGrades setting
            const showPendingGrades =
                result.department &&
                result.department.organization &&
                result.department.organization.instituteConfiguration &&
                result.department.organization.instituteConfiguration.showPendingGrades;
            if (showPendingGrades) {
                return context.model('StudentGrade').where("student").equal(self.getId()).prepare();
            } else {
                return context.model('StudentGrade').where("student").equal(self.getId()).and('status/alternateName').notEqual('pending').prepare();
            }
        });
    }
    /**
     *
     */
    @EdmMapping.func("Classes",EdmType.CollectionOf("StudentCourseClass"))
    getClasses() {
        return this.context.model('StudentCourseClass').where("student").equal(this.getId()).prepare();
    }

    /**
     *
     */
    @EdmMapping.func("currentRegistration","StudentPeriodRegistration")
    getCurrentRegistration() {
        const self = this;
        return self.getModel().select('id', 'department').expand('department').where('id').equal(self.id).getItem().then(function (result) {
            if (_.isNil(result)) {
                return new HttpNotFoundError();
            }
            return self.context.model('StudentPeriodRegistration')
                .where("student").equal(self.id).and("registrationYear").equal(result.department.currentYear)
                .and("registrationPeriod").equal(result.department.currentPeriod).prepare();
        });
    }

    /**
     *
     */
    @EdmMapping.param("data", "StudentPeriodRegistration", false, true)
    @EdmMapping.action("currentRegistration","StudentPeriodRegistration")
    commitCurrentRegistration(data) {
        let self = this;
        // get context
        let context = self.context;
        // convert registration to data object
        let registration = context.model("StudentPeriodRegistration").convert(data);
        return new Promise((resolve, reject) => {
            registration.is(':current').then(function (result) {
                if (result) {
                    //execute in unattended mode
                    context.unattended(function (cb) {
                            //do save registration
                            registration.save(context, function (err) {
                                cb(err);
                            });
                        }, function (err) {
                            if (err) {
                                TraceUtils.error(err);
                                return reject(new HttpServerError());
                            }
                            return resolve(registration);
                        });
                }
                else {
                    return reject(new HttpForbiddenError('Invalid registration data. The specified registration is not referred to current academic year and period.'));
                }
            }, function (err) {
                TraceUtils.error(err);
                return reject(new HttpServerError());
            });
        });

    }

    /**
     *
     */
    @EdmMapping.func("currentRegistrationStatus","StudentPeriodRegistration")
    getCurrentRegistrationStatus() {
        const self = this;
        return new Promise(function (resolve, reject) {
            try {
                return self.getModel().select('id', 'department', 'studentStatus').expand('department').where('id').equal(self.id).getItem().then(function (student) {
                    if (_.isNil(student)) {
                        return reject(new HttpNotFoundError());
                    }
                    // get periodRegistrationEffectiveStatuses
                    return self.context.model('PeriodRegistrationEffectiveStatus').getItems().then(function (statuses) {
                        //#1 first check if student is active
                        if (student.studentStatus.alternateName !== 'active') {
                            return resolve(statuses.find(function (x) {
                                    return x.code === 'INACTIVE_STUDENT'
                                })
                            );
                        }
                        //#2 check if registration period is open
                        const currentDate = new Date();
                        currentDate.setHours(0, 0, 0, 0);
                        let isRegistrationPeriod = false;
                        return self.context.model('LocalDepartment').where('id').equal(student.department.id)
                            .and('date(registrationPeriodStart)').lowerOrEqual(currentDate)
                            .and('date(registrationPeriodEnd)').greaterOrEqual(currentDate)
                            .select("id", "name", "registrationPeriodStart", "registrationPeriodEnd").flatten().silent().first(function (err, department) {
                                if (err) {
                                    return reject(new HttpError(err));
                                }
                                //check also if there is a referrer and ignore this error
                                if (_.isNil(department) && (!(self.action instanceof StudentRegisterActionModel))) {
                                    // registration period is closed
                                    return resolve(statuses.find(function (x) {
                                            return x.code === 'CLOSED_REGISTRATION_PERIOD'
                                        })
                                    );
                                }
                                else {
                                    //check also if database is online
                                    return self.context.model('Workspace').where('databaseStatus').equal('online').flatten().silent().first(function (err, workspace) {
                                        if (err) {
                                            return reject(new HttpError(err));
                                        }
                                        if (_.isNil(workspace)) {
                                            return reject(new HttpError('The system is updating. Please try again later.'));
                                        }
                                        else {
                                            isRegistrationPeriod = true;
                                            return self.canSelectSpecialty(function (err) {
                                                if (err) {
                                                    return self.context.model('StudentPeriodRegistration').where("student").equal(self.id).and("registrationYear").equal(student.department.currentYear)
                                                        .and("registrationPeriod").equal(student.department.currentPeriod).expand('classes').getItem().then(function (registration) {
                                                            if (_.isNil(registration)) {
                                                                return resolve(statuses.find(function (x) {
                                                                        return x.code === 'OPEN_NO_TRANSACTION'
                                                                    })
                                                                );
                                                            }
                                                            else {
                                                                //check registration status
                                                                if (registration.status.alternateName === 'closed') {
                                                                    //registration exists and is closed
                                                                    return resolve(statuses.find(function (x) {
                                                                            return x.code === 'CLOSED_REGISTRATION'
                                                                        })
                                                                    );
                                                                }
                                                                return self.context.model('StudentRegistrationDocument').where("registration").equal(registration.id).orderByDescending('dateCreated').first().then(function (lastDocument) {
                                                                    // there is no transaction
                                                                    if (!lastDocument) {
                                                                        //
                                                                        if (registration.classes.length > 0) {
                                                                            //secretary has registered classes
                                                                            return resolve(statuses.find(function (x) {
                                                                                    return x.code === 'P_SYSTEM_TRANSACTION'
                                                                                })
                                                                            );
                                                                        }
                                                                        return resolve(statuses.find(function (x) {
                                                                                return x.code === 'OPEN_NO_TRANSACTION'
                                                                            })
                                                                        );
                                                                    }
                                                                    switch (lastDocument.documentStatus.alternateName) {
                                                                        case "pending":
                                                                            return resolve(statuses.find(function (x) {
                                                                                    return x.code === 'PENDING_TRANSACTION'
                                                                                })
                                                                            );


                                                                        case "closed":
                                                                            return resolve(
                                                                                statuses.find(function (x) {
                                                                                    return x.code === 'SUCCEEDED_TRANSACTION'
                                                                                }));
                                                                        default:
                                                                            return resolve(statuses.find(function (x) {
                                                                                return x.code === 'FAILED_TRANSACTION'
                                                                            }));
                                                                    }
                                                                });
                                                            }
                                                        });
                                                }
                                                else {
                                                    return resolve(statuses.find(function (x) {
                                                            return x.code === 'SELECT_SPECIALTY'
                                                        })
                                                    );
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                    });
                });
            }
            catch (err) {
                return reject(err);
            }
        });
    }


    /**
     * @return {Promise<DataQueryable>}
     */
    @EdmMapping.func("availableSpecialties",EdmType.CollectionOf("StudyProgramSpecialty"))
    getAvailableSpecialties() {
        const self = this;
        return self.getModel().select('id', 'studyProgram').where('id').equal(self.id).flatten().getItem().then(function (result) {
            if (_.isNil(result)) {
                return new HttpNotFoundError();
            }
            return self.context.model('StudyProgramSpecialty').where("studyProgram").equal(result.program).prepare();
        });
    }


    availableProgramSpecialties(callback) {
        const self = this, context = self.context;
        self.programOf(function(err,program) {
            if (err) {
                TraceUtils.error(err);
                return callback(err);
            }
            if (_.isNil(program)) {
                return callback(new Error('Student program is null or undefined'));
            }
            context.model("ProgramSpecialty").where("program").equal(program).and("specialty").notEqual(-1).all(function (err, result) {
                if (err) {
                    TraceUtils.error(err);
                    return callback(err);
                }
                callback(null, result);
            });
        });
    }

    /**
     * @returns {Promise<DataQueryable>}
     */
    @EdmMapping.func('availableClasses',EdmType.CollectionOf("StudentAvailableClass"))
    getAvailableClasses() {
        const self = this;
        return self.getModel().select('id','department').where('id').equal(self.id).expand('department').getItem().then(function(result) {
            if (_.isNil(result)) {
                return Promise.reject(new HttpNotFoundError());
            }
            const entityExpr = util.format('ufnStudentAvailableClasses(%s,%s,%s)',result.department.currentYear, result.department.currentPeriod, result.id);
            const entity = new QueryEntity(entityExpr).as('StudentAvailableClasses');
            const q = self.context.model('StudentAvailableClass').asQueryable();
            //replace entity reference
            q.query.from(entity);
            //return queryable
            return Promise.resolve(q);
        }).catch(function(err) {
            TraceUtils.error(err);
            if (err instanceof HttpError) {
                return Promise.reject(err);
            }
            return Promise.reject(new HttpServerError());
        });
    }

    /**
     * @param {*} specialty
     */
    saveSpecialty(specialty) {

    }


    @EdmMapping.func("canSelectSpecialty")
    canSelectSpecialtyResult(){
        const self = this;
        const res={};
        return new Promise(function(resolve, reject) {
            try {
                self.canSelectSpecialty(function (err) {
                    if (err) {
                        res.success=false;
                        res.message=err.message;
                    }
                    else {
                        res.success=true;
                    }
                    return resolve(res);
                });
            }
            catch(err) {
                return reject(err);
            }

        });
    }


    canSelectSpecialty(done) {
        const self = this, context = self.context;
        const studentPeriodRegistrations = context.model('StudentPeriodRegistration');
        //validate student specialty
        self.attr('specialtyId', function (err, result) {
            if (err) {
                return done(err);
            }
            //check if student is already following a specialty
            if (LangUtils.parseInt(result) !== -1) {
                return done(new Error(context.__('Student is already following a program specialty.')));
            }
            //check student status
            self.is(':active').then(function (active) {
                if (!active) {
                    return done(new Error('Student is not active.'));
                }
                //check if student has registered to current period
                studentPeriodRegistrations.asQueryable().select('current').silent().flatten().first(function (err, registration) {
                    if (err) {
                        TraceUtils.error(err);
                        return done(new HttpServerError());
                    }
                    if (_.isNil(registration)) {
                        return done(new Error(context.__('You must first register to current semester.')));
                    }
                    //set registration periods
                    self.registrationYear = registration.registrationYear;
                    self.registrationPeriod = registration.registrationPeriod;
                    //check if student semester is greater or equal to selection semester of specialty
                    self.programOf(function (err, programId) {
                        context.model("StudyProgram").where('id').equal(programId).select('id', 'specialtySelectionSemester', 'specialties').silent().expand('specialties').first(function (err, program) {
                            if (err) {
                                return done(err);
                            }
                            if (_.isNil(program)) {
                                return done(new Error(context.__('Student program is null or undefined.')));
                            }
                            if (!_.isNil(program)["specialties"] &&  program["specialties"].length === 1) {
                                return done(new Error(context.__('Only one specialty found for this program.')));
                            }
                            const studentSemester = LangUtils.parseInt(registration["semester"]), selectionSemester = LangUtils.parseInt(program["specialtySelectionSemester"]);
                            if (selectionSemester > studentSemester) {
                                return done(new Error(util.format(context.__('You can select a specialty at %s semester. Student semester is %s'), selectionSemester, studentSemester)));
                            }

                            //validate department registration period
                            self.departmentOf(function (err, result) {
                                if (err) {
                                    return done(err);
                                }
                                if (_.isNil(result)) {
                                    return done(new Error(context.__('Student department is null or undefined')));
                                }
                                const currentDate = new Date();
                                currentDate.setHours(0, 0, 0, 0);
                                context.model('LocalDepartment').where('id').equal(result)
                                    .and('registrationPeriodStart').lowerOrEqual(currentDate)
                                    .and('registrationPeriodEnd').greaterOrEqual(currentDate)
                                    .select( "id", "name", "registrationPeriodStart", "registrationPeriodEnd", "allowSpecialtySelection" ).flatten().silent().first(function (err, department) {
                                        if (err) {
                                            return done(err);
                                        }
                                        if (_.isNil(department)) {
                                            return done(new Error(context.__('Invalid student registration period.')));
                                        }
                                        else {
                                            if (LangUtils.parseInt(department["allowSpecialtySelection"]) === 0) {
                                                return done(new Error(context.__('Student department does not allow changing specialty.')));
                                            }
                                            else {
                                                //check also if database is online
                                                context.model('Workspace').where('databaseStatus').equal('online').flatten().silent().first(function (err, workspace) {
                                                    if (err) {
                                                        return done(err);
                                                    }
                                                    if (_.isNil(workspace)) {
                                                        return done(new Error(context.__('The system is updating. Please try again later.')));
                                                    }
                                                    else {
                                                        done();
                                                    }
                                                });
                                            }
                                        }
                                    });
                            });

                        });
                    });

                });
            }).catch(function (err) {
                done(err);
            });
        });
    }

    /**
     * @swagger
     *
     * /api/Students/Me/RegisterActions:
     *  get:
     *    tags:
     *      - Student
     *    description: Returns a collection of student register actions
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              type: object
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    @EdmMapping.func('RegisterActions', EdmType.CollectionOf('StudentRegisterAction'))
    /**
     * @return DataQueryable
     */
    getRegisterActions() {
        return this.context.model('StudentRegisterAction').where('object').equal(this.getId()).prepare();
    }

    /**
     * @swagger
     *
     * /api/Students/Me/CurrentRegisterAction:
     *  get:
     *    tags:
     *      - Student
     *    description: Returns an object which represents a register action for current academic period
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *              schema:
     *                  $ref: '#/components/schemas/StudentRegisterAction'
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */

    /**
     * @returns {Promise<StudentRegisterAction>}
     */
    @EdmMapping.func('CurrentRegisterAction', 'StudentRegisterAction')
    async getCurrentRegisterAction() {
        // noinspection JSCheckFunctionSignatures
        let q = await this.context.model('StudentRegisterAction')
            .filter('registrationYear eq $it/object/department/currentYear and registrationPeriod eq $it/object/department/currentPeriod');
        return q.and('object').equal(this.getId()).prepare();
    }

    /**
     * @swagger
     *
     * /api/Students/Me/CurrentRegisterAction:
     *  post:
     *    tags:
     *      - Student
     *    description: Save student register action for current academic period
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *              schema:
     *                  $ref: '#/components/schemas/StudentRegisterAction'
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    /**
     * @returns {Promise<StudentRegisterAction|*>}
     */
    @EdmMapping.action('CurrentRegisterAction', 'StudentRegisterAction')
    async saveCurrentRegisterAction() {
        // get silent mode from parent
        const silent = this.getModel().isSilent();
        // get current academic year and period
        const student = await this.context.model('Student')
            .where('id').equal(this.getId())
            .select('id', 'department/currentYear as currentYear', 'department/currentPeriod as currentPeriod')
            .silent()
            .getItem();
        // set object equal to this
        const registerAction ={
            object: student.id,
            registrationYear: student.currentYear,
            registrationPeriod: student.currentPeriod
        };
        // save register action
        await this.context.model('StudentRegisterAction').silent(silent).save(registerAction);
        // return original register action
        return await this.context.model('StudentRegisterAction').where('id').equal(registerAction.id).silent(silent).getItem();
    }

}

module.exports = Student;
