import csv from 'fast-csv';
import moment from 'moment';
import {RandomUtils} from "@themost/common/utils";
import {Readable} from 'stream';
import fs from 'fs';
import path from 'path';
import {LangUtils} from "@themost/common/utils";

/**
 * @method Response~csv
 * @param {*} data
 * @returns Promise
 */
/**
 * Extends Request to send csv data
 * @param {FastCsvOptions=} options
 * @returns {RequestHandler}
 */
function csvParser(options) {
    // noinspection JSValidateTypes
    return function (req, res, next) {
        /**
         * @method
         * @name Response~csv
         * @this Response
         * @param {*} data
         * @returns Promise
         */
        res.csv = function(data) {
            /**
             * @type {Response}
             */
            let self = this;
            return new Promise(function (resolve, reject) {
                if (Array.isArray(data)) {
                    return csv.writeToString(data, Object.assign({
                        headers: true,
                        delimiter: ';',
                        rowDelimiter: '\r\n',
                        transform: function(row) {
                            let res = { };
                            Object.keys(row).forEach( key => {
                                // convert dates to string
                                if (row[key] instanceof Date) {
                                    res[key] = moment(row[key]).locale(req.locale).format('L LTS');
                                }
                                // if attribute is an object
                                else if (typeof row[key]==='object' && row[key] !== null) {
                                    // if attribute has property name
                                    if (row[key].hasOwnProperty('name')) {
                                        // return this name
                                        res[key] = row[key]['name'];
                                    }
                                    else {
                                        // otherwise return object
                                        res[key] = row[key];
                                    }
                                }
                                else {
                                    // set property value
                                    res[key] = row[key];
                                }
                            });
                            return res;
                        }
                    }, options), (err, data) => {
                        if (err) {
                            // reject with error
                            return reject(err);
                        }
                        // set status
                        self.status(200)
                            .set('content-type', 'application/csv')
                            .set('content-disposition', RandomUtils.randomChars(12).toUpperCase() + '.csv');
                        //set header
                        //write BOM - Byte Order Mark (for UTF-8)
                        self.write(new Buffer('EFBBBF', 'hex'));
                        // write string data
                        self.write(data, 'utf8');
                        // and finally return
                        return resolve();
                    });
                }
                // unprocessable entity
                self.status(422);
                // and finally return
                return resolve();
            });
        };
        return next();
    }
}
/**
 * Handles post of a *.csv and convert data to a valid collection of objects
 * @param {PostCsvOptions=} options
 * @returns {RequestHandler}
 */
function csvPostParser(options) {
    // noinspection JSValidateTypes
    return function (req, res, next) {
        // set defaults
        let opts = Object.assign({
            name: 'file',
            csv: {
                headers: false,
                delimiter: ';',
                rowDelimiter: '\n'
            }
        }, options);
        // get file key name
        let name = opts.name;
        if (req.hasOwnProperty(name)) {
            // try to parse csv
            if (req[name].mimetype === 'text/csv' || req[name].mimetype === 'application/csv') {
                let stream;
                if (req[name].buffer instanceof Buffer) {
                    stream = new Readable();
                    stream.push(req[name].buffer);
                    stream.push(null);
                }
                else {
                    stream = fs.createReadStream(path.resolve(req[name].destination, req[name].filename));
                }
                let body = [], headers = [], i, x;
                let csvStream = csv(opts.csv).on("data", function(data){
                    if (headers.length===0) {
                        // prepare headers
                        headers = data;
                    }
                    else {
                        x = { };
                        for ( i = 0; i < data.length; i++) {
                            if (headers[i]) {
                                // convert values
                                x[headers[i]] = data[i] ? LangUtils.convert(data[i]) : null;
                            }
                        }
                        body.push(x);
                    }
                })
                    .on("end", function() {
                        req.body = body;
                        return next();
                    });
                return stream.pipe(csvStream);
            }
        }
        return next();
    }
}

module.exports.csvParser = csvParser;
module.exports.csvPostParser = csvPostParser;