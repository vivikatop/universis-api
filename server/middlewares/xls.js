import {LangUtils, RandomUtils} from "@themost/common/utils";
import {Workbook} from 'exceljs';
import {Readable} from "stream";
import fs from "fs";
import path from "path";

const XlsxContentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'

/**
 * Extends Request to send xls data
 * @returns {RequestHandler}
 */
function xlsParser() {
    // noinspection JSValidateTypes
    return function (req, res, next) {
        /**
         * @method
         * @name Response~xls
         * @this Response
         * @param {*} data
         * @returns Promise
         */
        res.xls = function(data) {
            /**
             * @type {Response}
             */
            let self = this;
            return new Promise(function (resolve, reject) {
                if (Array.isArray(data)) {
                    let workbook  = new Workbook();
                    let sheet = workbook.addWorksheet('Sheet1');
                    let rows = data.map( row => {
                        let res = {};
                        Object.keys(row).forEach( key => {
                            // if attribute is an object
                            if (typeof row[key]==='object' && row[key] !== null) {
                                // if attribute has property name
                                if (row[key].hasOwnProperty('name')) {
                                    // return this name
                                    res[key] = row[key]['name'];
                                }
                                else {
                                    // otherwise return object
                                    res[key] = row[key];
                                }
                            }
                            else {
                                // set property value
                                res[key] = row[key];
                            }
                        });
                        return res;
                    });
                    // add columns
                    if (rows.length>0) {
                        sheet.columns = Object.keys(rows[0]).map( key => {
                            return { header: key, key: key };
                        });
                    }
                    rows.forEach( row => {
                        sheet.addRow(row);
                    });

                    // set status
                    self.status(200)
                        .set('content-type', XlsxContentType)
                        .set('content-disposition', RandomUtils.randomChars(12).toUpperCase() + '.xlsx');
                    return workbook.xlsx.write(self).then(()=> {
                        return resolve();
                    }).catch( err=> {
                        return reject(err);
                    });
                }
                // unprocessable entity
                self.status(422);
                // and finally return
                return resolve();
            });
        };
        return next();
    }
}

/**
 * Handles post of a *.xlsx and convert data to a valid collection of objects
 * @param {PostXlsOptions=} options
 * @returns {RequestHandler}
 */
function xlsPostParser(options) {
    // noinspection JSValidateTypes
    return function (req, res, next) {
        // set defaults
        let opts = Object.assign({
            name: 'file'
        }, options);
        // get file key name
        let name = opts.name;
        if (req.hasOwnProperty(name)) {
            // try to parse csv
            if (req[name].mimetype === XlsxContentType) {
                let stream;
                if (req[name].buffer instanceof Buffer) {
                    stream = new Readable();
                    stream.push(req[name].buffer);
                    stream.push(null);
                }
                else {
                    stream = fs.createReadStream(path.resolve(req[name].destination, req[name].filename));
                }
                let body = [], headers = [];
                // read from a file
                let workbook = new Workbook();
                return workbook.xlsx.read(stream)
                    .then(function() {
                        // read workbook
                        let sheet = workbook.getWorksheet(1);
                        sheet.eachRow((row, rowNumber) => {
                            if (rowNumber === 1) {
                                headers = row.values;
                            }
                            else {
                                let res = { };
                                row.values.forEach((v, k) => {
                                    if (k>0) {
                                        res[headers[k]] = v;
                                    }
                                });
                                body.push(res);
                            }
                        });
                        req.body = body;
                        return next();
                    }).catch( err=> {
                        return next(err);
                    });
            }
        }
        return next();
    }
}

module.exports.xlsParser = xlsParser;
module.exports.xlsPostParser = xlsPostParser;
module.exports.XlsxContentType = XlsxContentType;