import {LangUtils} from '@themost/common/utils';
import _ from 'lodash';
import util from 'util';
import {ValidationResult} from "../errors";
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    try {
        callback();
    }
    catch (e) {
        callback(e);
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    try {
        const target = event.target, context = event.model.context;
        if (event.state === 1 || event.state === 2) {

            let classes;
            if (util.isArray(target['classes'])) {
                classes = target['classes'].filter(function (x) {
                    return x.$state !== 4;
                });
            }
            else {
                return callback();
            }

            const filtered = classes.filter(function (x) {
                x.validationResult = x.validationResult || {};
                return x.validationResult.success === true;
            });
            if (filtered.length === 0) {
                 return callback();
             }
            //check semester rules
            let validationResults = [];
            let registrationPeriod = context.model('AcademicPeriod').convert(event.target.registrationPeriod).getId();
            let registrationYear = context.model('AcademicYear').convert(event.target.registrationYear).getId();
            let query = util.format("SELECT * FROM [dbo].StudentProgramSemesterRules (%s,%s,%s)", registrationYear, registrationPeriod, event.model.idOf(event.target["student"]));
            context.db.execute(query, null, function (err, result) {
                if (err) {
                    return callback(err);
                }
                else {
                    if (result.length > 0) {
                        let message;
                        for (let i = 0; i < result.length; i++) {
                            const numberOfCourses=LangUtils.parseInt(result[i]["numberOfCourses"]);
                            const numberOfUnits=LangUtils.parseInt(result[i]["numberOfUnits"]);
                            const numberOfHours=LangUtils.parseInt(result[i]["numberOfHours"]);
                            let registeredCourses= 0;
                            let registeredUnits= 0;
                            let registeredHours=0;

                            const filteredByType = filtered.filter(function (x) {
                                if (_.isObject(x.courseType))
                                    x.courseType = x.courseType.id;
                                if (x.courseType === result[i].courseType)
                                    return x;
                            });
                            if (result[i]["courseType"] === -1) {
                                registeredCourses = filtered.length;
                                for (let j = 0; j < filtered.length; j++) {
                                   if (LangUtils.parseBoolean(filtered[j]["calculatedInRegistration"])) {
                                       registeredUnits += filtered[j]["units"];
                                       registeredHours += filtered[j]["hours"];
                                   }
                                }
                            }
                            else {
                                registeredCourses = filteredByType.length;
                                for (let k = 0; k < filteredByType.length; k++) {
                                    if (LangUtils.parseBoolean(filteredByType[k]["calculatedInRegistration"])) {
                                        registeredUnits += filteredByType[k]["units"];
                                        registeredHours += filteredByType[k]["hours"];
                                    }
                                }
                            }
                            if (registeredCourses > numberOfCourses && numberOfCourses>0) {
                                message = util.format("Μπορείτε να δηλώσετε μέχρι [%s] μαθήματα τύπου <%s>", numberOfCourses, result[i]["courseTypeDescription"]);
                                validationResults.push(new ValidationResult(false, 'FAIL', message));
                            }
                            if (registeredUnits > numberOfUnits && numberOfUnits>0) {
                                message = util.format("Μπορείτε να δηλώσετε μέχρι [%s] διδακτικές μονάδες μαθήματων τύπου <%s>", numberOfUnits, result[i]["courseTypeDescription"]);
                                validationResults.push(new ValidationResult(false, 'FAIL', message));
                            }
                            if (registeredHours > registeredHours && registeredHours>0) {
                                message = util.format("Μπορείτε να δηλώσετε μέχρι [%s] ώρες μαθήματων τύπου <%s>", numberOfHours, result[i]["courseTypeDescription"]);
                                validationResults.push(new ValidationResult(false, 'FAIL', message));
                            }
                        }
                        if (validationResults.length > 0) {
                            const validationRes = new ValidationResult(false, 'FAIL', 'Η δήλωση μαθημάτων δεν είναι έγκυρη. Δε δηλώθηκε κανένα μάθημα.');
                            validationRes.validationResults = [];
                            validationRes.validationResults.push.apply(validationRes.validationResults, validationResults);
                            return callback(validationRes);
                        }
                        else
                            return callback();
                    }
                    else
                       return callback();
                }
            });
        }
        else {
            callback();
        }
    }
    catch (e) {
        callback(e)
    }
}