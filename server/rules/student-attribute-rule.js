import _ from 'lodash';
import {TraceUtils, LangUtils} from '@themost/common/utils';
import util from "util";
import Rule from "./../models/rule-model";

/**
 * @class
 * @augments Rule
 */
export default class StudentAttributeRule extends Rule {
    constructor(obj) {
        super('Rule', obj);
    }

    /**
     * Validates an expression against the current student
     * @param {*} obj
     * @param {function(Error=,*=)} done
     */
    validate(obj, done) {
        try {
            const self = this;
            const context = self.context;
            const students = context.model('Student'), student = students.convert(obj.student);
            if (_.isNil(student)) {
                return done(null, self.failure('ESTUD','Student data is missing.'));
            }
            if (_.isNil(student.getId())) {
                return done(null, self.failure('ESTUD','Student data cannot be found.'));
            }
            const op = this.operatorOf();
            if (_.isNil(op)) {
                return done(null, self.failure('EFAIL','An error occured while trying to validate rules.','The specified operator is not yet implemented.'));
            }
            //get queryable object
            const q = students.where('id').equal(student.getId()).prepare();
            const fnOperator = q[op];
            if (typeof fnOperator !== 'function') {
                return done(null, self.failure('EFAIL','An error occured while trying to validate rules.', 'The specified operator cannot be found or is invalid.'));
            }
            q.where(this.checkValues);
            fnOperator.apply(q, [ this.value1, this.value2 ]);
            q.silent().count(function(err, count) {
                if (err) {
                    TraceUtils.error(err);
                    return done(null, self.failure('EFAIL','An error occured while trying to validate rules.'));
                }
                const field = students.field(self.checkValues);
                //get minimum of prerequisites
                if (count === 1) {
                    //success
                    return done(null, self.success('SUCC',self.formatMessage(context.__(field.title), self.value1, self.value2)));
                }
                else {
                    //failure
                    done(null, self.failure('FAIL',self.formatMessage(context.__(field.title), self.value1, self.value2)));
                }
            });
        }
        catch(e) {
            done(e);
        }
    }

    formatMessage(p) {
        const op = LangUtils.parseInt(this.ruleOperator);
        let s;
        switch (op) {
            case 0:s = "Attribute [%s] must be equal to %s"; break;
            case 1:s = "Attribute [%s] must contain '%s"; break;
            case 2: s ="Attribute [%s] must be different from %s"; break;
            case 3: s = "Attribute [%s] must be greater than %s"; break;
            case 4: s = "Attribute [%s] must be lower than %s"; break;
            case 5: s = "Attribute [%s] must be between %s and %s"; break;
            case 6: s = "Attribute [%s] must start with '%s'"; break;
            case 7: s = "Attribute [%s] must not contain '%s'"; break;
            case 8: s = "Attribute [%s] must be greater or equal to %s"; break;
            case 9: s = "Attribute [%s] must be lower or equal to %s"; break;
        }
        const args = [].slice.call(arguments);
        s=this.context.__(s);
        args.unshift(s);
        for (let i = 0; i < args.length; i++) {
            if (_.isNil(args[i])) {
                args[i]='';
            }
        }
        return util.format.apply(this,args);
    }
}