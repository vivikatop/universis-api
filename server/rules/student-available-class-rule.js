import Rule from "./../models/rule-model";
/**
 * @class
 * @augments Rule
 */
export default class StudentCoursePassedRule extends Rule {
    constructor(obj) {
        super('Rule', obj);
    }

    /**
     * Validates an expression against the current student
     * @param {*} obj
     * @param {Function} done
     */
    validate(obj, done) {
        try {
            return done(null, this.success('SUCC','Rule passed'));
        }
        catch(e) {
            done(e);
        }
    }
}