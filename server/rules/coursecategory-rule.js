import _ from 'lodash';
import {TraceUtils, LangUtils} from '@themost/common/utils';
import util from "util";
import { QueryExpression } from '@themost/query/query';
import CourseAttribute from "./../models/course-attribute-rule";
import {EdmMapping} from "@themost/data";

@EdmMapping.entityType('Rule')
/**
 * @class
 * @augments CourseAttribute
 */
class CourseCategoryRule extends CourseAttribute {
    constructor() {
        super();
    }
    /**
     * Validates an expression against the current student
     * @param {*} obj
     * @param {Function} done
     */
    validate(obj, done) {
        try {
            //done(null, new ValidationResult(false,'FAIL','Class Registration was cancelled by the user'));
            const self = this;

            const context = self.context;
            const students = context.model('Student'), student = students.convert(obj.student), studentCourses=context.model('StudentCourse');
            if (_.isNil(student)) {
                return done(null, self.failure('ESTUD','Student data is missing.'));
            }
            if (_.isNil(student.getId())) {
                return done(null, self.failure('ESTUD','Student data cannot be found.'));
            }
            const op = this.operatorOf();
            if (_.isNil(op)) {
                return done(null, self.failure('EFAIL','An error occurred while trying to validate rules.','The specified operator is not yet implemented.'));
            }
            //get queryable object completed courses
            const values = this.checkValues.split(',');
            const q = studentCourses.where('student').equal(student.getId()).and('isPassed').equal(1).and('courseStructureType').in([1,4]).and('course/courseCategory').in(values).prepare();
            let fnOperator = q[op];
            if (typeof fnOperator !== 'function') {
                return done(null, self.failure('EFAIL','An error occured while trying to validate rules.', 'The specified operator cannot be found or is invalid.'));
            }

            const sumOf=this.value6;
            let fieldOf;

            switch (sumOf) {
                case "0":
                    fieldOf="sum(units) as res"; //units
                    break;
                case "2":
                    fieldOf="sum(hours) as res"; //hours
                    break;
                case "3":
                    fieldOf="sum(ects) as res"; //ects
                    break;
                default:
                    fieldOf="count(id) as res";
            }
            //exclude specific courses
            if (!_.isNil(this.value7)) {
                q.where("course").notIn(this.value7.split(',')).prepare();
            }
            //exclude specific course types
            if (!_.isNil(this.value8)) {
                q.where("courseType").notIn(this.value8.split(',')).prepare();
            }
            //check also course sector
            if (!_.isNil(this.value10)) {
                q.where("course/courseSector").in(this.value10.split(',')).prepare();
            }
            //check also course area
            if (!_.isNil(this.value11)) {
                q.where("course/courseArea").in(this.value11.split(',')).prepare();
            }

            q.silent().select(fieldOf).first (function(err, result) {
                if (err) {
                    TraceUtils.error(err);
                    return done(null, self.failure('EFAIL', 'An error occured while trying to validate rules.'));
                }
                const q2 = (new QueryExpression().from('a')).select([
                    { res: { $value: LangUtils.parseFloat(result.res) }}
                ]).where('res');
                q2.$fixed = true;
                fnOperator = q2[op];
                fnOperator.apply(q2, [ self.value1, self.value2 ]);
                studentCourses.context.db.execute(q2, null, function (err, result) {
                    if (err) {
                        return done(err);
                    }
                    else {
                        if (result.length === 1) {
                            self.formatDescription(function(err, message) {
                                if (err) { return done(err); }
                                return done(null, self.success('SUCC',message));
                            });
                        }
                        else {
                            self.formatDescription(function(err, message) {
                                if (err) { return done(err); }
                                return done(null, self.failure('FAIL',message));
                            });
                        }
                    }
                });

            });
        }
        catch(e) {
            done(e);
        }
    }

    formatDescription(callback) {
        const op = LangUtils.parseInt(this.ruleOperator);
        let s;
        let description;
        const self=this;
        const sumOf=this.value6;
        let fieldDescription;
        const args=[];

        switch (sumOf) {
            case "0":
                fieldDescription="Units of completed courses";
                break;
            case "2":
                fieldDescription="Hours of completed courses";
                break;
            case "3":
                fieldDescription="ECTS of completed courses";
                break;
            default:
                fieldDescription="Number of completed courses";
        }
        switch (op) {
            case 0:s = "%s must be equal to %s"; break;
            case 1:s = "%s must contain '%s'"; break;
            case 2: s = "%s must be different from %s"; break;
            case 3: s = "%s must be greater than %s"; break;
            case 4: s = "%s must be lower than %s"; break;
            case 5: s = "%s must be between %s and %s"; break;
            case 6: s = "%s must start with '%s'"; break;
            case 7: s = "%s must not contain '%s'"; break;
            case 8: s = "%s must be greater or equal to %s"; break;
            case 9: s = "%s must be lower or equal to %s"; break;
        }
        s=this.context.__(s);
        args.push(this.context.__(fieldDescription));
        args.push(this.value1);
        args.push(this.value2);
        for (let i = 0; i < args.length; i++) {
            if (_.isNil(args[i])) {
                args[i]='';
            }
        }
        args.unshift(s);
        s= util.format.apply(this,args);

        this.courseCategories(function(err,result) {
            if (err) {
                callback(err)
            }
            description = util.format("%s:%s, %s", self.context.__("Course categories"), result, s);
            self.ruleDescription(function (err, result) {
                if (err) {
                    callback(err)
                }
                description += result;
                callback(null, description);
            });
        });
    }

    courseCategories(callback) {
        try {
            const values = this.checkValues.split(',');
            this.context.model('CourseCategory').where('id').in(values).select(['id','name']).silent().all(function(err, result) {
                if (err) { return callback(err); }
                callback(null, result.map(function(x) {return util.format('%s', x.name)}).join(', '));
            });
        }
        catch(err) {
            callback(err);
        }
    }
}

module.exports = CourseCategoryRule;