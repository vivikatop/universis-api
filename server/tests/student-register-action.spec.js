/* eslint no-console: off */
import {assert} from 'chai';
import app from '../app';
import Institute from '../models/institute-model';

class CancelTransactionError extends Error {
    constructor() {
        super();
    }
}

/**
 * @this {DataAdapter}
 * @param {Function} func
 */
function executeInTransactionAsync(func) {
    return new Promise((resolve, reject) => {
        this.executeInTransaction((cb) => {
            try {
                func().then(() => {
                    return cb();
                }).catch( err => {
                    return cb(err);
                });
            }
            catch (err) {
                return cb(err);
            }

        }, err => {
            if (err) {
                return reject(err);
            }
        });
    });

}

describe('institute configuration', () => {

    /**
     * @type ExpressDataContext
     */
    let context;
    before(done => {
        /**
         * @type ExpressDataApplication
         */
        const testApp = app.get('ExpressDataApplication');
        // create context
        context = testApp.createContext();
        // set current student
        context.user = {
            name: 'user_20163877@example.com'
        };
        return done();
    });
    after(done => {
        if (context == null) {
            return done();
        }
        context.finalize(()=> {
            return done();
        });
    });

    it('should get register actions', async () =>  {
        /**
         * @type Student
         */
        let student = await context.model('Student').where('user/name').equal(context.user.name).getTypedItem();
        assert.isObject(student);
        let data = await student.getRegisterActions().getItems();
        assert.isArray(data);
    });

    it('should get current register', async () =>  {
        /**
         * @type Student
         */
        let student = await context.model('Student').where('user/name').equal(context.user.name).getTypedItem();
        assert.isObject(student);
        let registerAction = await (await student.getCurrentRegisterAction()).getItem();
        console.log('INFO', 'REGISTER ACTION', registerAction);
    });

    it('should add student register action', async () =>  {

        /**
         * @type Student
         */
        let student = await context.model('Student').where('user/name').equal(context.user.name).getTypedItem();
        try {
            await executeInTransactionAsync.bind(context.db)(async ()=> {
                let registerAction = await student.silent().saveCurrentRegisterAction();
                console.log('INFO','REGISTER ACTION', registerAction);
                assert.isObject(registerAction);
                // validate student
                assert.equal(registerAction.object.id, student.id);
                // validate student period registration
                let registration = student.getCurrentRegistration();
                assert.isObject(registration);
                // get register action
                let registerActions = await student.getRegisterActions().getItems();
                assert.isArray(registerActions);
                assert.isAtLeast(registerActions.length, 1);
                // cancel testing
                throw new CancelTransactionError();
            });
        }
        catch (err) {
            if (err instanceof CancelTransactionError) {
                return;
            }
            throw err;
        }
    });

});
