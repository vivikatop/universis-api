import onHeaders from 'on-headers';
/**
 * A middleware which completely disables cache control
 * @returns {Function}
 */
export function noCache() {
    return (req, res, next) => {
        onHeaders(res, ()=> {
            res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
            res.setHeader('Pragma', 'no-cache');
            res.setHeader('Expires', 0);
            // remove express js etag header
            res.removeHeader('ETag');
        });
        return next();
    }
}
