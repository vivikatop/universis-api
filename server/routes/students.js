/**
 * @license
 * Universis Project Version 1.0
 * Copyright (c) 2018, Universis Project All rights reserved
 *
 * Use of this source code is governed by an LGPL 3.0 license that can be
 * found in the LICENSE file at https://universis.io/license
 */
import express from 'express';
import Student from "../models/student-model";
import {HttpNotFoundError} from "@themost/common";
let router = express.Router();

router.get('/me/currentRegistration/effectiveStatus', (req, res, next) => {
    // execute async method
    async function getEffectiveStatus() {
        // get student
        let student = await Student.getMe(req.context).select('id').getTypedItem();
        // if student is undefined
        if (typeof student === 'undefined') {
            // throw not found exception
            throw new HttpNotFoundError();
        }
        // return current registration status
        return await student.getCurrentRegistrationStatus();
    }
    return getEffectiveStatus().then(registrationStatus => res.json(registrationStatus))
        .catch(reason => next(reason));
});

module.exports = router;