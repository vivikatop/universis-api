import express from 'express';
import {SwaggerService} from "../services/swagger-service";
import swaggerUi from 'swagger-ui-express';
import SwaggerParser from 'swagger-parser';
import {TraceUtils} from "@themost/common";
const YAML = SwaggerParser.YAML;

/**
 * @param {ConfigurationBase} configuration
 * @returns {Router}
 */
function apiDocs(configuration) {
    let router = express.Router();
    /* GET home page. */
    router.get('/schema', (req, res, next) => {
        // noinspection JSValidateTypes
        /**
         * @type {SwaggerService}
         */
        let swaggerService = req.context.getApplication().getStrategy(SwaggerService);
        if (typeof swaggerService === 'undefined') {
            return next(new Error('Invalid configuration. Swagger service cannot be found or is inaccessible.'));
        }
        let swaggerDocument = swaggerService.getSwaggerDocument(req.context);
        if (req.query.hasOwnProperty('yaml')) {
            return res.set('Content-Type', 'text/plain').send(YAML.stringify(swaggerDocument, 4));
        }
        return res.json(swaggerDocument);
    });

    // get swagger configuration from application settings/swagger e.g.
    // ...
    // "swagger":{
    //     "swaggerUrl": "schema",
    //         "swaggerOptions": {
    //         "oauth2RedirectUrl": "http://localhost:5001/api-docs/oauth2-redirect.html"
    //     }
    // }
    // ...
    let swaggerConfiguration = configuration.getSourceAt('settings/swagger');
    if (typeof swaggerConfiguration === 'undefined') {
        TraceUtils.error('Swagger configuration is missing or is inaccessible. Swagger UI will be configured with default settings. Security schemes may not be working.')
    }
    let options = Object.assign({
        swaggerUrl: 'schema'
    }, swaggerConfiguration);

    router.use('/', swaggerUi.serve, swaggerUi.setup(null, options));
    // return router
    return router
}

module.exports = apiDocs;