import express from 'express';
import {getEntityFunction, getEntitySetFunction} from '@themost/express/middleware';
import Instructor from "../models/instructor-model";
import {HttpConflictError, HttpForbiddenError, HttpNotFoundError} from "@themost/common";
import multer from 'multer';
import {csvPostParser} from '../middlewares/csv';
import {PrivateContentService} from "../services/content-service";
import path from 'path';
import {ValidationResult} from "../errors";
import {xlsPostParser, XlsxContentType} from "../middlewares/xls";

let router = express.Router();

// set multer storage
const upload = multer({ dest: 'content/user/' });

function readStream(stream) {
    return new Promise((resolve, reject) => {
        let buffers = [];
        stream.on('data', (d) => {
            buffers.push(d);
        });
        stream.on('end', () => {
            return resolve(Buffer.concat(buffers));
        });
        stream.on('error', (err) => {
            return reject(err);
        });
    });
}

/**
 * @returns {RequestHandler}
 */
function bindInstructor() {
    return (req, res, next) => {
        // get instructor
        Instructor.getMe(req.context).then( getInstructor => {
            return getInstructor.getTypedItem().then ( instructor => {
                // set instructor to current request params
                req.params.instructor = instructor;
                return next();
            });
        }).catch((err) => {
            return next(err);
        });
    };
}

/**
 * @swagger
 *
 * /api/Instructors/Me/Classes/Students:
 *  get:
 *    tags:
 *      - Instructor
 *    description: Returns a collection of students which have been registered in one or more instructor's courses
 *    security:
 *      - OAuth2:
 *          - teachers
 *    responses:
 *      '200':
 *        description: success
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *      '403':
 *        description: forbidden
 *      '404':
 *        description: not found
 *      '500':
 *        description: internal server error
 */
router.get('/me/classes/students', function getClassesStudents (req, res, next) {
    // set entity set parameters
    Object.assign(req.params, {
        entitySetFunction: "me",
        entityFunction : "classStudents"
    });
    // drop $select query parameter
    delete req.query.$select;
    // go to entitySetFunction middleware
    return next();
}, getEntitySetFunction({
    entitySet: "Instructors",
}));

/**
 * @swagger
 *
 * /api/Instructors/Me/Classes/{courseClass}/Students/Export:
 *  get:
 *    tags:
 *      - Instructor
 *    description: Exports a collection of students who have been registered to the given course class
 *    parameters:
 *      - in: path
 *        name: courseClass
 *        description: A variant which represents the id of a course class
 *        schema:
 *          type: string
 *        required: true
 *    security:
 *      - OAuth2:
 *          - teachers
 *    responses:
 *      '200':
 *        description: success
 *        content:
 *          application/csv:
 *            schema:
 *              type: array
 *              items:
 *                  type: object
 *          application/vnd.openxmlformats-officedocument.spreadsheetml.sheet:
 *            schema:
 *              type: array
 *              items:
 *                  type: object
 *      '404':
 *        description: not found
 *      '500':
 *        description: internal server error
 */
router.get('/me/classes/:courseClass/students/export', bindInstructor(), async function exportCourseClassStudents (req, res, next) {
    try {
        /**
         * get instructor object
         * @type {Instructor}
         */
        let instructor = req.params.instructor;
        // check if instructor is undefined
        if (typeof instructor === 'undefined') {
            // and throw error
            return next(new HttpForbiddenError());
        }
        /**
         * get instructor course class
         * @type {CourseClass}
         */
        let courseClass = await instructor.getInstructorClasses().where('id').equal(req.params.courseClass).select('id').getTypedItem();
        // check if course exam is undefined
        if (typeof courseClass === 'undefined') {
            // and throw error
            return next(new HttpNotFoundError('Instructor course class cannot be found'));
        }
        // apply custom filter
        // get only $filter and $orderby query params
        let getStudents = await req.context.model('StudentCourseClass').filter({
            $filter: req.query.$filter,
            $orderby: req.query.$orderby
        });
        // set course class filter
        getStudents.prepare().and('courseClass').equal(courseClass.id);
        /**
         * get export view
         * @type {DataModelView}
         */
        let view = getStudents.model.getDataView('Export');
        // get student data
        let students = await getStudents.select('Export').getAllItems();
        // validate content type
        if (req.accepts(XlsxContentType)) {
            // return xlsx
            return res.xls(view.toLocaleArray(students)).then(() => res.end());
        }
        // return csv
        return res.csv(view.toLocaleArray(students)).then(() => res.end());
    }
    catch (err) {
        return next(err);
    }
});

/**
 * @description A request handler for setting course class student view for instructors.
 * This operation blocks instructors from getting more data than they are permitted to view.
 */
router.get('/me/classes/:courseClass/students/?$',  async function setCourseClassStudentParams(req, res, next) {
    // set $select query option to export
    req.query.$select="InstructorClassStudents";
    // return
    return next();
});


router.get('/me/classes/:courseClass/:courseClassFunction', function getCourseClassFunction (req, res, next) {
    // get instructor course class
    Instructor.getMe(req.context).then(q => {
        // get instructor object
        return q.select('id').getTypedItem().then(
            /**
             * @param {Instructor} instructor
             */
            instructor =>  {
                // check if instructor is undefined
                if (typeof instructor === 'undefined') {
                    // and continue
                    return next(new HttpNotFoundError());
                }
                return instructor.getInstructorClasses().where('id').equal(req.params.courseClass).select('id').getTypedItem().then(
                    /**
                     * @param {CourseClass} courseClass
                     */
                    courseClass=> {
                        // check if course class is undefined
                        if (typeof courseClass === 'undefined') {
                            // and continue
                            return next(new HttpNotFoundError());
                        }
                        // call middleware
                        return getEntityFunction({
                            entityFunctionFrom: 'courseClassFunction',
                            from: 'courseClass',
                            entitySet: 'CourseClasses'
                        })(req, res, next);
                    });
            }).catch(reason => {
            return next(reason);
        });
    });
});
/**
 * @description A request handler for setting course exam student view for instructors.
 * This operation blocks instructors from getting more data than they are permitted to view.
 */
router.get('/me/exams/:courseExam/students/?$',  async function getCourseExamStudents(req, res, next) {
    // set $select query option for instructor

    if (req.query.$group == null && req.query.$groupby == null)
    {
        req.query.$select="CourseExamInstructorView";
    }
    // return
    return next();
});

/**
 * @swagger
 *
 * /api/Instructors/Me/Exams/{courseExam}/Students/Export:
 *  get:
 *    tags:
 *      - Instructor
 *    description: Exports a collection of students which are eligible to get a grade for a course exam
 *    parameters:
 *      - in: path
 *        name: courseExam
 *        description: An integer which represents the id of a course exam
 *        schema:
 *          type: integer
 *        required: true
 *    security:
 *      - OAuth2:
 *          - teachers
 *    responses:
 *      '200':
 *        description: success
 *        content:
 *          application/csv:
 *            schema:
 *              type: array
 *              items:
 *                  type: object
 *          application/vnd.openxmlformats-officedocument.spreadsheetml.sheet:
 *            schema:
 *              type: array
 *              items:
 *                  type: object
 *      '404':
 *        description: not found
 *      '500':
 *        description: internal server error
 */
router.get('/me/exams/:courseExam/students/export', async function exportCourseExamStudents (req, res, next) {
    try {
        let getInstructor = await Instructor.getMe(req.context);
        /**
         * get instructor object
         * @type {Instructor}
         */
        let instructor = await getInstructor.select('id').getTypedItem();
        // check if instructor is undefined
        if (typeof instructor === 'undefined') {
            // and throw error
            return next(new HttpNotFoundError());
        }
        /**
         * get instructor course exam
         * @type {CourseExam}
         */
        let courseExam = await instructor.getInstructorExams().where('id').equal(req.params.courseExam).select('id').getTypedItem();
        // check if course exam is undefined
        if (typeof courseExam === 'undefined') {
            // and throw error
            return next(new HttpNotFoundError());
        }
        let getStudents = await courseExam.getStudents();
        /**
         * get export view
         * @type {DataModelView}
         */
        let view = getStudents.model.getDataView('export');
        // get student data
        let students = await getStudents.select('export').getAllItems();
        // validate content type
        if (req.accepts(XlsxContentType)) {
            // return xlsx
            return res.xls(view.toLocaleArray(students)).then(() => res.end());
        }
        // return csv
        return res.csv(view.toLocaleArray(students)).then(() => res.end());
    }
    catch (err) {
        return next(err);
    }
});
/**
 * @swagger
 *
 *  /api/Instructors/Me/Exams/{courseExam}/Students/Import:
 *   post:
 *    tags:
 *      - Instructor
 *    description: Uploads a *.csv or *.xlsx file which contains a collection of student grades.
 *    security:
 *     - OAuth2:
 *        - teachers
 *    parameters:
 *      - in: path
 *        name: courseExam
 *        description: An integer which represents the id of a course exam
 *        schema:
 *          type: integer
 *        required: true
 *    requestBody:
 *      content:
 *        multipart/form-data:
 *          schema:
 *            type: object
 *            properties:
 *              file:
 *                type: string
 *                format: binary
 *    responses:
 *      '200':
 *        description: success
 *        content:
 *          application/json:
 *              schema:
 *                  $ref: '#/components/schemas/ExamDocumentUploadAction'
 *      '404':
 *        description: not found
 *      '500':
 *        description: internal server error
 */
router.post('/me/exams/:courseExam/students/import', upload.single('file'),
    csvPostParser({ name: 'file' }),
    xlsPostParser({ name: 'file' }),
    async function importCourseExamStudents (req, res, next) {

    try {
        let getInstructor = await Instructor.getMe(req.context);
        /**
         * get instructor object
         * @type {Instructor}
         */
        let instructor = await getInstructor.select('id').getTypedItem();
        // check if instructor is undefined
        if (typeof instructor === 'undefined') {
            // and throw error
            return next(new HttpNotFoundError());
        }
        /**
         * get instructor course exam
         * @type {CourseExam}
         */
        let courseExam = await instructor.getInstructorExams().where('id').equal(req.params.courseExam).expand('status').getTypedItem();
        // check if course exam is undefined
        if (typeof courseExam === 'undefined') {
            // and throw error
            return next(new HttpNotFoundError());
        }
        if (courseExam.status.alternateName==='closed')
        {
            return next(new HttpForbiddenError('Course exam is not accessible due to its state.'));
        }
        /**
         * get student export view
         * @type {DataModelView}
         */
        let view = req.context.model('CourseExamStudentGrade').getDataView('export');
        // get students
        courseExam.grades = view.fromLocaleArray(req.body);
        // run pre check
        await courseExam.preCheck();
        if (courseExam.validationResult.success === false) {
            // send validation failure
            return res.status(409).json({
                object: courseExam,
                actionStatus: {
                    "alternateName": "FailedActionStatus"
                }
            });
        }
        // create course exam document action
        let documentAction = {
            object: courseExam
        };
        // save exam document action and append request file as action attachment
        await req.context.model('ExamDocumentUploadAction').on('after.save', (event, done) => {
            /**
             * @type *
             */
            let context = event.model.context;
            // add attachment
            /**
             * get private content service
             * @type {PrivateContentService}
             */
            let service = context.getApplication().getService(PrivateContentService);
            //se attachment attributes from multer file
            let attachment = {
                contentType: req.file.mimetype,
                name: req.file.originalname
            };
            // add attachment (in unattended mode)
            context.unattended((cb) => {
                service.copyFrom(context, path.resolve(req.file.destination, req.file.filename), attachment, (err)=> {
                    try {
                        if (err) {
                            return cb(err);
                        }
                        let target = event.model.convert(event.target);
                        // add attachment to document action
                        return target.property('attachments').insert(attachment).then(()=> {
                            return cb();
                        }).catch( err => {
                            return cb(err);
                        });
                    }
                    catch (err) {
                        return cb(err);
                    }

                });
            }, (err) => {
                return done(err);
            });
        }).silent().save(documentAction);
        // assign course exam data
        Object.assign(documentAction, {
           object: courseExam
        });
        // return data from document action
        return res.json(documentAction);
    }
    catch (err) {
        return next(err);
    }
});
 /**
 * @swagger
 *
 * /api/Instructors/Me/Exams/{courseExam}/Actions/:action/Complete:
 *   post:
 *    tags:
 *      - Instructor
 *    description: Uploads a *.csv or *.xlsx file which contains a collection of student grades.
 *    security:
 *      - OAuth2:
 *          - teachers
 *    parameters:
 *      - in: path
 *        name: courseExam
 *        description: An integer which represents the id of a course exam
 *        schema:
 *          type: integer
 *        required: true
 *      - in: path
 *        name: action
 *        description: An integer which represents the id of an exam document upload action
 *        schema:
 *          type: integer
 *        required: true
 *    responses:
 *      '200':
 *        description: success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExamDocumentUploadAction'
 *      '409':
 *        description: conflict
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExamDocumentUploadAction'
 *      '404':
 *        description: not found
 *      '403':
 *        description: forbidden
 *      '500':
 *        description: internal server error
 */
router.post('/me/exams/:courseExam/actions/:action/complete', bindInstructor(), async function completeCourseExamDcoumentAction (req, res, next) {
    let documentAction;
    try {
        let instructor = req.params.instructor;
        if (typeof instructor === 'undefined') {
            return next(new HttpForbiddenError());
        }
        /**
         * get instructor course exam
         * @type {CourseExam}
         */
        let courseExam = await instructor.getInstructorExams().where('id').equal(req.params.courseExam).expand('status').getTypedItem();

        //set course exam status to completed
        courseExam.status = await req.context.model('CourseExamStatus').where('alternateName').equal('completed').getTypedItem();
        // set current user as completedByUser
        courseExam.completedByUser=instructor.user;

        if (typeof courseExam === 'undefined') {
            return next(new HttpNotFoundError('Course exam cannot be found or is inaccessible'));
        }
        // validate course exam status (other than closed)
        if (courseExam.status.alternateName === 'closed') {
            return next(new HttpConflictError('Course exam has invalid status.'));
        }
        // get document action
        documentAction = await courseExam.getDocumentActions().where('id').equal(req.params.action).getItem();
        if (typeof documentAction === 'undefined') {
            return next(new HttpNotFoundError('Document action cannot be found or is inaccessible'));
        }
        // validate action status
        if (documentAction.actionStatus.alternateName !== 'ActiveActionStatus') {
            return next(new HttpConflictError('Document action has invalid status.'));
        }
        documentAction.actionStatus = {
            "alternateName": "CompletedActionStatus"
        };
        await req.context.model('ExamDocumentUploadAction').on('before.save', (event, done) => {
            async function beforeSaveDocumentAction(event) {
                let target = event.model.convert(event.target);
                // get exam document action attachment
                let attachment = await target.property('attachments').silent().getItem();
                if (typeof attachment === 'undefined') {
                    throw new HttpNotFoundError('Document action attachment is missing.');
                }
                /**
                 * get private content service
                 * @type {PrivateContentService}
                 */
                let service = event.model.context.getApplication().getService(PrivateContentService);
                /**
                 * get attachment readable stream
                 * @type {ReadableStream}
                 */
                let stream = await new Promise((resolve, reject) => {
                    event.model.context.unattended((cb)=> {
                        service.createReadStream(event.model.context, attachment, (err, result) => {
                            return cb(err, result);
                        });
                    }, (err, result) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve(result);
                    });
                });
                let body;
                let buffer = await readStream(stream);
                // parse text/csv
                if (attachment.contentType === 'text/csv' || attachment.contentType === 'application/csv') {
                    body = await new Promise((resolve, reject) => {
                        // create a request like object
                        let req = {
                            file: {
                                buffer: buffer,
                                mimetype: "text/csv"
                            }
                        };
                        // call csvPostHandler to parse csv
                        csvPostParser({
                            name: "file"
                        })(req, null, (err) => {
                            if (err) {
                                reject(err);
                            }
                            // get body
                            return resolve(req.body);
                        });
                    });
                }
                // parse application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
                else if (attachment.contentType === XlsxContentType) {
                    body = await new Promise((resolve, reject) => {
                        // create a request like object
                        let req = {
                            file: {
                                buffer: buffer,
                                mimetype: XlsxContentType
                            }
                        };
                        // call csvPostHandler to parse csv
                        xlsPostParser({
                            name: "file"
                        })(req, null, (err) => {
                            if (err) {
                                reject(err);
                            }
                            // get body
                            return resolve(req.body);
                        });
                    });
                }
                if (body) {
                    /**
                     * get student export view
                     * @type {DataModelView}
                     */
                    let view = req.context.model('CourseExamStudentGrade').getDataView('export');
                    // set course exam grades
                    courseExam.grades = view.fromLocaleArray(body);
                    // save course exam
                    return await new Promise((resolve, reject) => {
                        event.model.context.unattended((cb)=> {
                            courseExam.save(event.model.context, (err) => {
                                return cb(err);
                            });
                        }, (err) => {
                            if (err) {
                                return reject(err);
                            }
                            if (courseExam.validationResult.success === false) {
                                // restore documentAction data
                                documentAction.actionStatus = {
                                    "alternateName": "ActiveActionStatus"
                                };
                                documentAction.object = courseExam;
                                return reject(courseExam.validationResult);
                            }
                            return resolve();
                        });
                    });
                }
                throw new Error('Source content type is not yet implemented');
            }
            beforeSaveDocumentAction(event).then(()=> {
                // set course exam
                event.target.object = courseExam;
                // get identifier form courseExamDocument
                return event.model.context.model('CourseExamDocument').where('id').equal(courseExam.docId).silent().getItem().then(document=> {
                   if (document)
                   {
                       event.target.additionalResult=document.identifier;
                   }
                    return done();
                });
            }).catch( err=> {
               return done(err);
            });
        }).silent().save(documentAction);
        // set course exam object
        documentAction.object = courseExam;
        //get course exam result document
        if (documentAction.additionalResult) {
            documentAction.additionalResult = await req.context.model('CourseExamDocument').where('identifier').equal(documentAction.additionalResult).silent().getItem();
        }
        // return document action
        return res.json(documentAction);
    }
    catch(err) {
        if (err instanceof ValidationResult) {
            if (documentAction) {
                return res.status(409).json(documentAction);
            }
            return res.status(409).json(err);
        }
        return next(err);
    }
});

/**
 * @swagger
 *
 * /api/Instructors/Me/Exams/{courseExam}/Actions/:action/Cancel:
 *   post:
 *    tags:
 *      - Instructor
 *    description: Cancels a document upload action associated with the given course exam
 *    security:
 *      - OAuth2:
 *          - teachers
 *    parameters:
 *      - in: path
 *        name: courseExam
 *        description: An integer which represents the id of a course exam
 *        schema:
 *          type: integer
 *        required: true
 *      - in: path
 *        name: action
 *        description: An integer which represents the id of an exam document upload action
 *        schema:
 *          type: integer
 *        required: true
 *    responses:
 *      '200':
 *        description: success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExamDocumentUploadAction'
 *      '409':
 *        description: conflict
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExamDocumentUploadAction'
 *      '404':
 *        description: not found
 *      '403':
 *        description: forbidden
 *      '500':
 *        description: internal server error
 */
router.post('/me/exams/:courseExam/actions/:action/cancel', bindInstructor(), async function cancelCourseExamDocumentAction (req, res, next) {
    try {
        let instructor = req.params.instructor;
        if (typeof instructor === 'undefined') {
            return next(new HttpForbiddenError());
        }
        /**
         * get instructor course exam
         * @type {CourseExam}
         */
        let courseExam = await instructor.getInstructorExams().where('id').equal(req.params.courseExam).getTypedItem();
        if (typeof courseExam === 'undefined') {
            return next(new HttpNotFoundError('Course exam cannot be found or is inaccessible'));
        }
        // validate course exam status (other than closed)
        if (courseExam.status.alternateName === 'closed') {
            return next(new HttpConflictError('Course exam has invalid status.'));
        }
        // get document action
        let documentAction = await courseExam.getDocumentActions()
            .where('id').equal(req.params.action)
            .and('owner').notEqual(null)
            .and('owner').equal(instructor.user)
            .getItem();
        if (typeof documentAction === 'undefined') {
            return next(new HttpNotFoundError('Document action cannot be found or is inaccessible'));
        }
        if (documentAction.actionStatus.alternateName !== 'ActiveActionStatus') {
            return next(new HttpConflictError('Document action cannot cancelled due to its state'));
        }
        // change status
        documentAction.actionStatus = {
            alternateName: "CancelledActionStatus"
        };
        await req.context.model('ExamDocumentUploadAction').silent().save(documentAction);
        // return updated document action
        return res.json(documentAction);
    }
    catch (err) {
        return next(err);
    }
});

router.get('/me/exams/:courseExam/:courseExamFunction', function getCourseExamFunction (req, res, next) {
    // get instructor course class
    Instructor.getMe(req.context).then(q => {
        // get instructor object
        return q.select('id').getTypedItem().then(
            /**
             * @param {Instructor} instructor
             */
            instructor =>  {
                // check if instructor is undefined
                if (typeof instructor === 'undefined') {
                    // and continue
                    return next(new HttpForbiddenError());
                }
                return instructor.getInstructorExams().where('id').equal(req.params.courseExam).select('id').getTypedItem().then(
                    /**
                     * @param {CourseExam} courseExam
                     */
                    courseExam=> {
                        // check if course exam is undefined
                        if (typeof courseExam === 'undefined') {
                            // and continue
                            return next(new HttpNotFoundError());
                        }
                        // call middleware
                        return getEntityFunction({
                            entityFunctionFrom: 'courseExamFunction',
                            from: 'courseExam',
                            entitySet: 'CourseExams'
                        })(req, res, next);
                    });
            }).catch(reason => {
            return next(reason);
        });
    });
});
/**
 * @swagger
 * /api/Instructors/Me/Theses/Students:
 *  get:
 *    tags:
 *      - Instructor
 *    description: Returns a collection of students who have been assigned one or more instructor's theses
 *    security:
 *      - OAuth2:
 *          - teachers
 *    responses:
 *      '200':
 *        description: success
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *      '403':
 *        description: forbidden
 *      '404':
 *        description: not found
 *      '500':
 *        description: internal server error
 */
router.get('/me/theses/students', function getThesesStudents (req, res, next) {
    // set entity set parameters
    Object.assign(req.params, {
        entitySetFunction: "me",
        entityFunction : "thesisStudents"
    });
    // drop $select query parameter
    delete req.query.$select;
    // go to entitySetFunction middleware
    return next();
}, getEntitySetFunction({
    entitySet: "Instructors",
}));

module.exports = router;
