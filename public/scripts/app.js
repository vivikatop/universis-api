/* eslint-disable no-var */
var app = angular.module('docs', ['ngSanitize', 'ui.router']);

app.controller('SidebarController', function($http) {
// eslint-disable-next-line no-invalid-this
    var vm = this;
    function activate() {
        $http.get('api/').then(function (response) {
            vm.endpoints = response.data.value;
        });
    }
    activate();
});

app.controller('EntityTypesController', function($http) {
// eslint-disable-next-line no-invalid-this
    var vm = this;
    function activate() {
        // get swagger document
        $http.get('/api-docs/schema').then(function (response) {
            // get entity types
            vm.entityTypes = Object.keys(response.data.components.schemas).sort(function (a,b) {
                return a < b ? -1: (a > b ? 1 : 0);
            }).map(function(key) {
                return {
                    "name": key,
                    "description": response.data.components.schemas[key].description
                }
            });
        });
    }
    activate();
});

app.controller('EntityTypeController', function($http, $state, $stateParams) {
// eslint-disable-next-line no-invalid-this
    var vm = this;
    function activate() {
        // get swagger document
        $http.get('/api-docs/schema').then(function (response) {
            // get entity type
            var entityType = response.data.components.schemas[$stateParams.entityType];
            // map entity type
            vm.entityType = {
                "name": $stateParams.entityType,
                "description": entityType.description,
                "properties": Object.keys(entityType.properties).sort(function (a,b) {
                    return a < b ? -1: (a > b ? 1 : 0);
                }).map(function(key) {
                   var property = entityType.properties[key];
                   return Object.assign({}, property, {
                       "name": key,
                       "required": entityType.required.indexOf(key)>=0
                   }, {
                       "$ref": property.$ref ? property.$ref.replace('#/components/schemas/','') : null
                   })
                })
            };
        });
    }
    activate();
});

app.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider.state({
        name: 'index',
        url: '/',
        templateUrl: 'components/index.component.html'
    }).state({
        name: 'entityTypes',
        url: '/entityTypes',
        templateUrl: 'components/entity-types.component.html'
    }).state({
        name: 'entityTypeDetails',
        url: '/entityTypes/:entityType',
        templateUrl: 'components/entity-type.component.html'
    });

    $urlRouterProvider.otherwise('/');
});
