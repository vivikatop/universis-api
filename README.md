# UniverSIS
[UniverSIS](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

This repository provides information on the API methods and the schema models.

## How to install
  Check out the installation instructions in [INSTALL.md](INSTALL.md)

## Documentation

1. API methods and schema models

  https://api.universis.io/api-docs/

1. Check out our wiki for more information our features

  https://gitlab.com/universis/universis-api/wikis/home

1. Services
Summary of available routes

  * Instructors:
  https://gitlab.com/universis/universis-api/blob/master/server/routes/instructors.md
